# This file may be used to create an environment using:
# $ install miniconda or anaconda
# $ conda create --name <env_name> -c bioconda --file <this file>
# then
# $ source activate <env_name>
# or
# $ source deactivate
graphviz=2.38.0
python=3.5.1
samtools=1.2
snakemake=3.5.5
pyyaml=3.11
seqtk=1.2
cutadapt=1.10
picard=2.3.0
# NOTE: trim_galore, bowtie2, bismark, PileOMeth, bgzip, tabix are needed as
# well, first check if they are available in bioconda
pandas=0.18.1
requests=2.10.0
beautifulsoup4=4.5.1
lxml=3.6.4
