data:
  root:
    "../../data/arabidopsis_thaliana/"
  ann: "genome_annotation/bed12/"
  ass: "genome_assembly/TAIR10.fasta"
  chr: "genome_assembly/TAIR10_chromSize.txt"
  cns: "dna_regulatory-regions/AtCNS_noChr.bed.gz"
  cst: "chromatin/"
  gff: "raw/araport/Araport11_noChr.gff.gz"
  gtf: "genome_annotation/gtf/Araport11_GFF3_genes_transposons.201606.gtf.gz"
  imp: "raw/publications/wyder2017-unpublished/ImprintedGenes_Athaliana_LerCol_TAIR10_noNA.bed"
  kee: "chromatin/KEEs.bed"
  ori: "dna_replication-origins/costas2011_ori.txt"
  pro: "raw/publications/tokizawa2017-the_plant_journal/tpj13511-sup-0004-DataS3.xlsx"
  trp: "raw/from_Araport/TAIR10_genome_release/annotation/gff/transposons/TAIR10_Transposable_Elements.txt.gz"
  utr: "raw/publications/tokizawa2017-the_plant_journal/tpj13511-sup-0003-DataS2.txt"
  vcf: "dna_variants/arabidopsis_thaliana.vcf.gz"
params:
  file:
    bed6:
      names:
        [chrom, start, end, name, score, strand]
      dtype:
        [str, int, int, str, float, str]
    div:
      names:
        ['#chrom', start, end, JSD_bit_, 'sample size', HMIX_bit_, 5mC, C]
      dtype:
        [str, int, int, float, int, float, int, int]
    gff:
      names:
        [seqid, source, type, start, end, score, strand, phase, attributes]
      dtype:
        [str, str, str, int, int, float, str, float, str]
    interMR:
      names:
        ['#chrom', start, end, num, CpG.MMC, CpG.HMC, CHG.MMC, CHG.HMC, CHH.MMC, CHH.HMC]
      dtype:
        [str, int, int, int, int, int, int, int, int, int]
  cytosine:
    context:
      [CpG, CHG, CHH]
    type:
      [HMC, MMC, MSC]
    ubound(pct): 80
    lbound(pct): 20
    maxmergedistance(bp): [5, 20]
    minlength(bp): [5, 20]
  genome:
    bins: [1000, 10000, 50000]
    chromosome:
      nuclear: ['1', '2', '3', '4', '5']
      cytoplasmic: [Mt, Pt]
    ecotype: Col-0
    genes:
        MEDEA: AT1G02580
        MLP28: AT1G70830
        TT10: AT5G48100
        KNAT7: AT1G62990
        PI4Kc3: AT5G24240
        AGL42: AT5G62165
    genotype: wt
    signal: [jsd, met]
    skew:
      window_size(bp): 200
      window_step(bp): 10
    source:
      - aerial-part
      - embryo
      - endosperm
      - immature-flower-buds
      - inflorescence
      - root
      - rosette
      - shoot
      - sperm-cell
      - vegetative-nucleus
      - whole-organism
    specific:
      - embryo
      - endosperm
      - immature-flower-buds
      - root
      - rosette
      - sperm-cell
      - vegetative-nucleus
    specific_short:
      - embryo
      - endosperm
      - flower bud
      - root
      - rosette
      - sperm
      - veg. nucleus
  shannon:
    chunk:
    columns:
    names:
  plot:
    color:
      CpG: "#1b9e77"
      CHG: "#d95f02"
      CHH: "#7570b3"
      MSC: "#ca0020"
      MMC: "#f4a582"
      HMC: "#0571b0"
    deeptools:
      binsize(bp): [10000]
      computeMatrix_genes:
        regions: [araport11_transposable_element_gene, imprinted_gene, nonTEnonImprint_gene]
        params: --metagene -b 2000 -a 2000 -m 4000 --binSize 100
      computeMatrix_chromstate:
        params: -b 2000 -a 2000 -m 4000 --binSize 100
        threads: 4
      plotProfile_genes: --perGroup --regionsLabel 'TE Genes' 'Imprinted Genes' 'Remaining Genes' --samplesLabel CpG CHG CHH
      plotProfile_chromstate: --perGroup --regionsLabel 1 2 3 4 5 6 7 8 9 --samplesLabel CpG CHG CHH --numPlotsPerRow 3 --startLabel start --endLabel end
      computeMatrix_nonTEgenes:
        params: -b 2000 -a 2000 -m 4000 --binSize 100
        threads: 4
      plotHeatmap_nonTEgenes: --samplesLabel CpG CHG CHH --kmeans 4 --whatToShow 'heatmap and colorbar'
  vcf2bed:
    memory: 6G
    variants: [snvs, insertions, deletions]
  region:
    bed:
      - araport11_novel_transcribed_region
      - araport11_protein_coding
      - araport11_pseudogene
      - araport11_transposable_element_gene
      - cns_3UTR
      - cns_5UTR
      - cns_ambiguous
      - cns_downstream
      - cns_intergenic
      - cns_intronic
      - cns_snc
      - cns_upstream
      - exon
      - genomebin_1000bp
      - genomebin_10000bp
      - genomebin_50000bp
      - imprinted_gene
      - intergenic_all
      - intergenic_coding
      - intron
      - knot_engaged_element_10000bp
      - knot_engaged_element_50000bp
      - max5primeUTR
      - nonTEnonImprint_gene
      - nonTE_protein_coding
      - origin_of_replication
      - rna_gene_antisense_lncRNA
      - rna_gene_lnc_RNA
      - rna_gene_miRNA_primary_transcript
      - rna_gene_snoRNA
      - rna_gene_snRNA
    gff:
      - promoter_all
      - promoter_orphan
      - promoter_genic
      - transposable_element
