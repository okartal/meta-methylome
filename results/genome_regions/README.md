# Genome regions

Genome regions are bed, gtf, or gff files that associate a stretch of DNA
(longer than 1 bp) with a feature or score.