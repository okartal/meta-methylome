# Metada produced by script extract_metadata.py

## Input metadata

  - metadata_CpG.csv
  - metadata_CHG.csv
  - metadata_CHH.csv

## Changes to metadata

  replacement['genome_source'] = {'rosette leaf': 'rosette'}
  replacement['genotype'] = {'wild type': 'wt'}

## Parameters for extraction

  config = dict(
      ecotype = "Col-0",
      genotype = "wt",
      min_count = 3,
  )

  * min_count is the minimum population sample size that has to be met in order for the genome source to be considered.
