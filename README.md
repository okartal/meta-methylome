# Cell Type-specific Genome Scans of DNA Methylation Divergence Indicate an Important Role for Transposable Elements

This repository contains metadata and the data analysis pipeline for the re-analysis of published methylomes using Jensen-Shannon divergence.