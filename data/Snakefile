# run this from the data directory
import datetime

# script directory relative to working directory (i.e. data/)
DIR = "../src/scripts/"

TODAY = datetime.date.today().strftime ("%Y-%m-%d")

ATTRIBUTES = ['sample_accession', 'experiment_accession']

wildcard_constraints:
    attr = "|".join(ATTRIBUTES)

rule end:
    input:
        expand("{date}.metadata.columbia.txt", date=TODAY),
        expand("{date}.metadata.all.txt", date=TODAY)

rule fetch_runs:
    output:
        temp("runs.txt")
    params:
        taxon = "\'Arabidopsis thaliana\'"
    shell:
        DIR + "fetch_runs.py --taxon {params.taxon} > {output} "

rule fetch_meta:
    input:
        "runs.txt"
    output:
        temp("metadata.{attr}.txt")
    params:
        attr = "{attr}"
    shell:
        DIR + "fetch_meta.py {input} {params.attr} > {output}"

rule merge_meta:
    input:
        runs = "runs.txt",
	samples = "metadata.sample_accession.txt",
	experiments = "metadata.experiment_accession.txt"
    output:
        "{date}.metadata.all.txt"
    shell:
        DIR + "merge_meta.py {input.runs} {input.samples} {input.experiments} {output}"

rule filter_meta:
    input:
        met = "{date}.metadata.all.txt",
        rex = "regex_filters.json",
        agg = "aggregated_columns.json"
    output:
        protected("{date}.metadata.columbia.txt")
    shell:
        DIR + "filter_meta.py {input.met} {input.rex} {input.agg} {output}"
