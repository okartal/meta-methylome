# Metadata from public repositories

* filename format: \<date\>\_\<database\>\_\<recordtype\>.csv 
* data from ArrayExpress used to get info on samples for BS-seq libraries
  from bioproject ID prjeb5331

To fetch, filter and merge metadata run snakemake. This produces
  - DATE.metadata.all.txt, and
  - DATE.metadata.columbia.txt
