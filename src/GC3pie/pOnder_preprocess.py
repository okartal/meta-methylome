#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Marc W Schmid (marcschmid@gmx.ch)
"""
A script to preprocess BSseqData (alignment and extraction).
"""

__version__ = 'development version (SVN $Revision$)'
__changelog__ = """
  2016-08-17:
  * Initial version
"""
__author__ = 'Marc Schmid <marcschmid@gmx.ch>'
__docformat__ = 'reStructuredText'

if __name__ == "__main__":
    import pOnder_preprocess
    pOnder_preprocess.PreprocessStagedScript().run()

import os
import sys
import shutil

from pkg_resources import Requirement, resource_filename

import gc3libs
import gc3libs.exceptions
from gc3libs import Application, Run, Task
from gc3libs.cmdline import SessionBasedScript, executable_file
import gc3libs.utils
from gc3libs.quantity import Memory, kB, MB, MiB, GB, Duration, \
    hours, minutes, seconds
from gc3libs.workflow import StagedTaskCollection


class BismarkSEApplication(Application):
    """An Application to run Bismark_SE.
    Arguments:
        - prefix: Prefix - eg the sample ID
        - reads: Fastq with reads (.gz), with local dir
        - outDir: Local directory for the results
        - isNotDirectional [False]: see --non_directional in bismark guide
        - isPBAT [False]: see --pbat in bismark guide
    """
        
    def __init__(self,
                 prefix,
                 reads,
                 outDir,
                 isNotDirectional=False,
                 isPBAT=False):
        self.required_cores = 4
        
        inputs = [reads]
        outputs = [ # output names are given in the bismark_SE script
            prefix + ".dupMarked.sorted.bam",
            prefix + ".dupMarked.sorted.bam.bai",
            prefix + "_markdup_metrics.txt",
            prefix + "_std_bismark.txt"
        ]

        notDirOp = " --notDirectional" if isNotDirectional else ""
        pbatOp = " --pbat" if isPBAT else ""
        threadOp = " -t %d" % (self.required_cores)
        args = "bismark_SE.sh%s%s%s" % (notDirOp, pbatOp, threadOp)
        args += " $PWD"
        args += " $PWD"
        args += " " + prefix
        args += " " + os.path.basename(reads)
        args += " /home/ubuntu/TAIR10"

        Application.__init__(
            self, arguments=args,
            inputs=inputs,
            outputs=outputs,
            output_dir=outDir + ".align",
            stdout=outputs[-1],
            join=True,
            requested_cores=self.required_cores,
            requested_memory=self.required_cores*4*GB,
            requested_walltime=48*hours)
    

class BismarkPEApplication(Application):
    """An Application to run Bismark_PE.
    Arguments:
        - prefix: Prefix - eg the sample ID
        - forwardReads: Fastq with forward reads (.gz), with local dir
        - reverseReads: Fastq with reverse reads (.gz), with local dir
        - outDir: Local directory for the results
        - isNotDirectional [False]: see --non_directional in bismark guide
        - isPBAT [False]: see --pbat in bismark guide
    """
        
    def __init__(self,
                 prefix,
                 forwardReads,
                 reverseReads,
                 outDir,
                 isNotDirectional=False,
                 isPBAT=False):
        self.required_cores = 4
        
        inputs = [forwardReads, reverseReads]
        outputs = [ # output names are given in the bismark_PE script
            prefix + ".dupMarked.sorted.bam",
            prefix + ".dupMarked.sorted.bam.bai",
            prefix + "_markdup_metrics.txt",
            prefix + "_std_bismark.txt"
        ]

        notDirOp = " --notDirectional" if isNotDirectional else ""
        pbatOp = " --pbat" if isPBAT else ""
        threadOp = " -t %d" % (self.required_cores)
        args = "bismark_PE.sh%s%s%s" % (notDirOp, pbatOp, threadOp)
        args += " $PWD"
        args += " $PWD"
        args += " " + prefix
        args += " " + os.path.basename(forwardReads)
        args += " " + os.path.basename(reverseReads)
        args += " /home/ubuntu/TAIR10"
        
        Application.__init__(
            self, arguments=args,
            inputs=inputs,
            outputs=outputs,
            output_dir=outDir + ".align",
            stdout=outputs[-1],
            join=True,
            requested_cores=self.required_cores,
            requested_memory=self.required_cores*4*GB,
            requested_walltime=48*hours)


class bamToBedGraphApplication(Application):
    """An Application to run bamToBedGraph.
    Arguments:
        - prefix: Prefix - eg the sample ID
        - outDir: Local directory for the results
        - bamFile ['']: BAM file with the alignments from bismark, with local dir
        - baiFile ['']: the index for the BAM file (.bam.bai), with local dir
        >> if no bamFile/baiFile supplied, it will guess it using the prefix and outDir.
    """
        
    def __init__(self,
                 prefix,
                 outDir,
                 bamFile='',
                 baiFile=''):
        self.required_cores = 1
        
        if not bamFile:
            bamFile = os.path.join(outDir + ".align", prefix+".dupMarked.sorted.bam")
            baiFile = os.path.join(outDir + ".align", prefix+".dupMarked.sorted.bam.bai")
        
        contexts = ["CpG", "CHG", "CHH"]
        inputs = [bamFile, baiFile]
        outputs = [] # output names are given in the bamToBedGraph.sh script
        outputs.extend([prefix + "_" + x + ".bedGraph.gz" for x in contexts])
        outputs.extend([prefix + "_" + x + ".bedGraph.gz.tbi" for x in contexts])
        outputs.extend([prefix + "_" + x + "_mbias.tab" for x in contexts])
        outputs.append(prefix + "_std_bamToBedGraph.txt")

        args = "bamToBedGraph.sh -t %d" % (self.required_cores)
        args += " $PWD"
        args += " $PWD"
        args += " " + prefix
        args += " " + os.path.basename(bamFile)
        args += " /home/ubuntu/TAIR10/At.fasta"

        Application.__init__(
            self, arguments=args,
            inputs=inputs,
            outputs=outputs,
            output_dir=outDir + ".count",
            stdout=outputs[-1],
            join=True,
            requested_cores=self.required_cores,
            requested_memory=self.required_cores*4*GB,
            requested_walltime=12*hours)

   
class PreprocessStagedTaskCollection(StagedTaskCollection):
    """ Runs first bismark_SE/PE.sh and then bamToBedGraph.sh.
    Arguments:
        - prefix: Prefix - eg the sample ID
        - forwardReads: Fastq with forward reads (.gz), with local dir
        - reverseReads: Fastq with reverse reads (.gz), with local dir
            >> or empty if single-end
        - outDir: Local directory for the results (a folder, for each sample, two additional folders will be created within this directory)
        - isNotDirectional [False]: see --non_directional in bismark guide
        - isPBAT [False]: see --pbat in bismark guide
    """
    def __init__(self,
                 prefix,
                 forwardReads,
                 reverseReads,
                 outDir,
                 isNotDirectional=False,
                 isPBAT=False,
                 **extra_args):
        self.my_pre = prefix
        self.my_for = forwardReads
        self.my_rev = reverseReads
        self.my_out = outDir
        self.my_IND = isNotDirectional
        self.my_PBA = isPBAT
        self.isPE = reverseReads != ""
        self.output_dir = outDir
        StagedTaskCollection.__init__(self)
            
    def stage0(self):
        """
        Stage0: bismark_SE/PE.sh
        """
        if self.isPE:
            return BismarkPEApplication(self.my_pre,
                                        self.my_for,
                                        self.my_rev,
                                        self.my_out,
                                        self.my_IND,
                                        self.my_PBA)
        return BismarkSEApplication(self.my_pre,
                                    self.my_for,
                                    self.my_out,
                                    self.my_IND,
                                    self.my_PBA)

    def stage1(self):
        """
        Stage1: bamToBedGraph.sh
        """
        return bamToBedGraphApplication(self.my_pre, self.my_out)

class PreprocessStagedScript(SessionBasedScript):
    """
    Run bismark_SE/PE.sh and bamToBedGraph.sh on a collection of samples.
    """

    def __init__(self):
        SessionBasedScript.__init__(self, version = __version__)

    def setup_args(self):
        self.add_param('input_csv', type=str,
                       help="CSV-file with six columns (no header!): \
                       prefix, forwardReads, reverseReads, outDir, isNotDirectional, isPBAT\
                       the last two should contain 'yes' or 'no'\
                       ")
        
    def parse_args(self):
        try:
            assert os.path.isfile(self.params.input_csv), \
                "Input .csv file %s not found." % self.params.input_csv                
        except AssertionError as ex:
            raise ValueError(ex.message)

    def new_tasks(self, extra):
        """
        Run PreprocessStagedTaskCollection for each line in the csv
        """
        
        with open(self.params.input_csv, 'r') as infile:
            metaTab = [line[:-1].split(',') for line in infile]
        
        tasks = []
        for argList in metaTab:
            extra_args = extra.copy()
            tasks.append(PreprocessStagedTaskCollection(argList[0],
                                                        argList[1],
                                                        argList[2],
                                                        os.path.join(argList[3], argList[0]),
                                                        argList[4] == "yes",
                                                        argList[5] == "yes",
                                                        **extra_args))
        
        return tasks
