# Source files for scripts and workflows

* scripts/
  
  - fetch metadata
  - filter data
  - plotting
  
* GC3pie/
  
  - align reads
  - extract methylation counts
  
* notebooks/

  - document metadata curation
  - exploratory analysis
  
* workflows/

  Snakemake workflows for subtasks 
