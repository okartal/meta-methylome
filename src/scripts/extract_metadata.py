#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Fix entries and extract metadata for each genome source.

usage: run the script in ./meta-methylome/results/metadata/
"""

import sys
from pathlib import Path

import pandas as pd
import yaml

### INPUT ###

metadata = "metadata_CpG.csv metadata_CHG.csv metadata_CHH.csv".split()

# fix

replacement = dict()
replacement['genome_source'] = {'rosette leaf': 'rosette'}
replacement['genotype'] = {'wild type': 'wt'}

# extract

config = dict(
    ecotype = "Col-0",
    genotype = "wt",
    min_count = 3,
)

### PROCESS ###

for infile in metadata:

    meta = pd.read_csv(infile, header=0)

    for field, mapping in replacement.items():
        for source, target in mapping.items():
            meta[field][meta[field] == source] = target

    query = " and ".join(["ecotype == '{}'".format(config['ecotype']),
                      "genotype == '{}'".format(config['genotype'])])

    meta_queried = meta.query(query)

    input_path = Path(infile)

    file_components = [str(input_path.stem), config['ecotype'], config['genotype'], "{suffix}"]

    filename = '.'.join(s.replace(' ', '-') for s in file_components)

    sources = list()

    for key, group in meta_queried.groupby('genome_source'):
        if len(group) >= config['min_count']:
            source = key.replace(' ', '-')
            suffix = source + input_path.suffix
            output_path = input_path.parent / Path(filename.format(suffix=suffix))
            group.to_csv(str(output_path), header=True, index=False)
            sources.append(source)

config['genome source'] = sources

with open('config.yaml', 'w') as configfile:
    yaml.dump(config, configfile)
