#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Utility functions.
"""

import pandas as pd

def chunker(seq, size):
    """Split a sequence into chunks.
    """
    return (seq[pos:pos + size] for pos in range(0, len(seq), size))

def get_samples(infile=None, attribute=None):
    """Get list of samples from table for the given attribute.
    """

    data = pd.read_table(infile, header=0)
    samples = data[attribute].unique()

    return samples

def get_attributes(xml, xpath='./BioSample/Attributes', attribute='attribute_name'):
    """Extracts attributes and values from an XML.

    Note
    ----
    default values for xpath and attribute are for fetching biosamples from ncbi
    """

    root = ET.fromstring(xml)

    for tag in root.findall(xpath):
        yield {child.attrib[attribute]: child.text for child in tag}
