#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
import os
import sys

import pandas as pd

def cli():
    """
    """

    parser = argparse.ArgumentParser(prog='make_categories')
    parser.add_argument('input', nargs='+')
    parser.add_argument('--output', '-o', required=True)
    args = parser.parse_args()

    return args

def cat(args):
    """
    """

    categories = [
        ('methylation level',
         'mlevel',
         [0., 0.2, 0.4, 0.6, 0.8, 1.],
         ['low', 'moderately low', 'intermediate', 'moderately high', 'high']),
        ('JSD',
         'mstate',
         [0., 0.2, 0.8, 1.],
         ['stable', 'indifferent', 'metastable'])
        ]

    for data in args.input:
        df = pd.read_csv(data, sep='\t', header=0, compression='infer',
                         dtype={'#chrom': 'category', 'context': 'category'})

        df['methylation level'] = (df['E']/(df['E'] + df['C'])).round(2)

        for score, category, bins, labels in categories:
            if score in set(df.columns):
                df[category] = pd.cut(df[score], bins, labels=labels,
                                      include_lowest=True)

        need_header = not os.path.isfile(args.output)

        (df.groupby(['context', 'mstate', 'mlevel'])
         .size()
         .to_csv(args.output, mode='a', header=need_header))

    pass

if __name__ == '__main__':
    args = cli()
    cat(args)
