#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Functions to fetch data from ENA and NCBI databases.
"""


import io
import re
import xml.etree.ElementTree as ET

import pandas as pd
import requests

from Bio import Entrez
from bs4 import BeautifulSoup
from utils import chunker


def ena_metadata(samples=None, attribute=None):
    """Fetches XML metadata from ENA and processes them into a DataFrame.
    """

    CHUNK_SIZE = 100

    url = "http://www.ebi.ac.uk/ena/data/view/{query}&display=xml"

    sample_records = list()

    chunk = chunker(samples, CHUNK_SIZE)

    for sample_ids in chunk:
        url_query = url.format(query=','.join(sample_ids))
        res = requests.get(url_query)
        if res.status_code == requests.codes.ok:
            soup = BeautifulSoup(res.text, "lxml")
            query_text = attribute.split("_")[0]
            items = soup.find_all(query_text)
            for sample in items:
                record = dict()
                record['accession'] = sample['accession']
                record['title'] = sample.title.contents[0]
                if sample.description:
                    record['additional_description'] = sample.description.contents[0]
                if attribute == "sample_accession":
                    for attr in sample.sample_attributes.find_all('sample_attribute'):
                        record[attr.tag.contents[0]] = attr.value.contents[0]

                sample_records.append(record)

    metadata = pd.DataFrame(sample_records)

    if not len(metadata) == len(samples):
        print('Mismatch between number of queried samples and returned records!',
              file=sys.stderr)
        sys.exit()
    else:
        return metadata


def ena_run(taxon=None):
    """Fetches ENA read runs and returns a DataFrame.
    """

    url = 'http://www.ebi.ac.uk/ena/data/warehouse/search'

    code = dict()
    # plants
    code['Arabidopsis thaliana'] = 3702
    code['Marchantia polymorpha'] = 3197
    code['Boechera stricta'] = 72658
    code['Physcomitrella patens'] = 3218
    code['Zea mays'] = 4577
    # mammals
    code['Homo sapiens'] = 9606
    code['Mus musculus'] = 10090

    try:
        tax_id = code[taxon]
    except KeyError as e:
        print('Input error: Taxon ', e, ' is unknown. Output file empty!',
              file=sys.stderr)
        sys.exit()

    query = list()
    query.append('tax_eq({})'.format(tax_id))
    query.append('instrument_platform="ILLUMINA"')
    query.append('(library_strategy="Bisulfite-Seq" OR library_strategy="OTHER")')
    query.append('library_source="GENOMIC"')

    fields = list()
    fields.append('study_accession')
    fields.append('submission_accession')
    fields.append('sample_accession')
    fields.append('secondary_sample_accession')
    fields.append('experiment_accession')
    fields.append('run_accession')
    fields.append('instrument_model')
    fields.append('library_strategy')
    fields.append('library_layout')
    fields.append('fastq_ftp')
    fields.append('submitted_ftp')

    params = dict()
    params['download'] = 'txt'
    params['result'] = 'read_run'
    params['display'] = 'report'
    params['query'] = ' AND '.join(query)
    params['fields'] = ','.join(fields)

    response = requests.get(url, params=params)

    runs = pd.read_table(io.StringIO(response.text), header=0)
    runs.rename(columns={'secondary_sample_accession': 'sample_accession',
                         'sample_accession': 'biosample_id'},
                inplace=True)

    return runs


def ena_submitter(study_accession):
    """Fetch submitters of studies using ENA REST API.
    """

    query = str.upper(",".join(study_accession))

    url = "http://www.ebi.ac.uk/ena/data/view/{0}%26display%3Dxml".format(query)

    response = requests.get(url)

    if response.ok:
        field = ["accession", "center_name"]
        root = ET.fromstring(response.text)
        for project in root:
            yield (project.attrib[field[0]], project.attrib[field[1]])
    else:
        print('FAIL')
        pass


def ncbi_attributes(accessions, db=None):
    """Fetch full text report and extract attributes.

    Arguments
    ---------
    - accessions: collection of accessions
    - db: NCBI database [default: 'biosample']

    Returns
    -------
    - iterator object that yields dictionaries with the attribute key and value

    Note
    ----
    This is the only method that works for now and gives nice results when read
    into a pandas.DataFrame. However, it is a bit slow for many accessions.
    """

    url = 'https://www.ncbi.nlm.nih.gov/{0}/'.format(db)

    params = dict()
    params['report'] = 'full'
    params['format'] = 'text'

    regex = re.compile('/(.*)="(.*)"')

    for acc in accessions:
        params['term'] = acc
        response = requests.get(url, params=params)
        if response.ok:
            match = regex.findall(response.text)
            match.append(('{0}_id'.format(db), acc))
            yield {attrib: value for attrib, value in match}


def ncbi_eutils(accessions, db=None):
    """Fetch XML from NCBI using eutils.

    Arguments
    ---------
    - accession: a list of accessions from the chosen database
    - db: a string giving the name of NCBI Entrez database

    Returns
    -------
    - iterator over XML text string

    Note
    ----
    eutils is buggy, does return wrong samples
    """

    url = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi'

    params = dict()
    params['db'] = db
    params['id'] = accessions
    params['rettype'] = 'xml'
    params['retmode'] = 'xml'

    response = requests.get(url, params=params)

    return response.text


def ncbi_entrez(accessions, db=None, email=None):
    """Fetch XML from NCBI using eutils Entrez from Biopython.

    Arguments
    ---------
    - accession: a list of accessions from the chosen database
    - db: a string giving the name of NCBI Entrez database

    Returns
    -------
    - iterator over XML text string

    Note
    ----
    this would be probably the preferred method over ncbi_eutils. But I tried
    both and neither of them is useful as long as eutils gives wrong results.
    """

    Entrez.email = email

    with Entrez.einfo() as handle:
        einfo = Entrez.read(handle)

    assert db in set(einfo['DbList'])

    with Entrez.efetch(db=db, id=accessions,
                       rettype="xml", retmode="xml") as query:
        response = query.read()

    return response
