#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Split bed6 file by name.

@author: Oender Kartal
"""
import sys

import pandas as pd

HEADER = "chrom chromStart chromEnd name score strand".split()

bed = pd.read_table(sys.argv[1], header=None, names=HEADER)

out_folder = sys.argv[2]
out_file = out_folder + "region_{key}.bed"

for name, data in bed.groupby("name"):
    data.to_csv(out_file.format(key=name), sep='\t', header=False, index=False)