#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Merge run, sample, and experiment metadata.
"""

__author__ = "Önder Kartal"
__version__ = "0.1"

import sys
import pandas as pd

def merge_meta(rpath, spath, epath):
    '''Merge metada on accessions.
    '''

    runs = pd.read_table(rpath, header=0)
    samples = pd.read_table(spath, header=0)
    experiments = pd.read_table(epath, header=0)

    merged = (runs
              .merge(samples, how='outer',
                     left_on="sample_accession", right_on="accession")
              .merge(experiments, how='outer',
                     left_on="experiment_accession", right_on="accession")
              .T.drop_duplicates().T)

    return merged

if __name__ == '__main__':

    merged_data = merge_meta(sys.argv[1], sys.argv[2], sys.argv[3])

    merged_data.to_csv(sys.argv[4], sep='\t', encoding='utf-8', index=False)

