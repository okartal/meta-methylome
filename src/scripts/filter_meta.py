#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Filter data based on regex and aggregated columns.
"""

__author__ = "Önder Kartal"
__version__ = "0.1"

import sys
import json
import pandas as pd

def filter_meta(meta_path, re_path, agg_path, output_path):
    '''Filter metadata.
    '''

    # read metadata
    meta = pd.read_table(meta_path, header=0)

    with open(re_path, "r") as re_json:
        pattern = json.load(re_json)

    with open(agg_path, "r") as agg_json:
        aggregated_colum = json.load(agg_json)

    # make new boolean columns in metadata
    for condition, regex in pattern.items():
        meta[condition] = (meta
                           .apply(lambda x: x.str.contains(regex, case=False),
                                  axis=1)
                           .any(axis=1))

    meta['genotype_missing'] = ~meta[aggregated_colum['genotype']].any(axis=1)

    # filter metadata
    # note: we retain runs if the genotype info is missing as these are assumed
    # to be col-0
    # note: grep -iv "ler\|landsberg\|cvi\|capverde\|c24"
    cond = (meta['has_bisulfite']
            & (meta['has_columbia'] | meta['genotype_missing'])
            & ~(meta['has_c24'] | meta['has_cvi'] | meta['has_ler'])
            )

    meta[cond].to_csv(output_path, sep='\t', encoding='utf-8', index=False)

    pass

if __name__ == '__main__':
    filter_meta(*sys.argv[1:])
