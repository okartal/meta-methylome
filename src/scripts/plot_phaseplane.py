#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Plot JSD vs MET phase plane.

Takes a divergence file streamed from a tabix query (stdin) and plots the
phase plane as a jointplot of JSD vs MET by partitioning the space with
hexagonal bins.

Usage:  tabix -h [OPTIONS] DIV_FILE [REGION] | plot_phaseplane.py COLOR FIG_FILE

Notes:
    - put COLOR in quotes
    - suffix of FIG_FILE determines format (.svg or .png)
"""

import sys

import matplotlib
matplotlib.use('Agg')

import pandas as pd
import seaborn as sns

from scipy.stats import spearmanr

sns.set(style='ticks')
sns.set_context('talk')

BINS = 'log'
GRIDSIZE = 100
MINCNT = 10
SPACE = 0

div_file = sys.stdin
color = sys.argv[1]
fig_file = sys.argv[2]

data = pd.read_table(div_file, header=0, usecols=[0, 3, 6, 7])

data['methylation level'] = data['5mC'] / (data['5mC'] + data['C'])

data.rename(columns={'JSD_bit_': 'JSD (bit)', "methylation level": "MET"}, inplace=True)

g = sns.jointplot('MET', 'JSD (bit)', data=data, kind='hex',
                  color=color, stat_func=spearmanr, space=SPACE,
                  joint_kws=dict(gridsize=GRIDSIZE, bins=BINS, mincnt=MINCNT))

g.savefig(fig_file)
