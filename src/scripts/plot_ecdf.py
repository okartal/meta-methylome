#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
import math
import sys

import numpy as np
import pandas as pd

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import seaborn as sns

sns.set_style("white")
sns.set_context("paper")

__version__ = "1.0"
__author__ = "Oender Kartal"

def cli():

    parser = argparse.ArgumentParser(prog='plot_ecdf', description = plot_ecdf.__doc__)

    # parser.usage = 'python %(prog)s [-h] [-v] [-b FORMAT] [-n FACTOR] -s COL:NAME -f COL:NAME FILE > OUTPUT'

    parser.add_argument('-v', '--version', action='version',
                        version='%(prog)s {0}'.format(__version__))
    parser.add_argument('-f', '--format', default='pdf',
                        help='output format (default: pdf)')
    parser.add_argument('-n', '--normalize', metavar='FACTOR', type=str,
                        choices=['log2e', 'log10e'],
                        help='normalizing factor for score (just added for convenience)')
    parser.add_argument('-s', '--score', nargs='+', metavar='COL', required=True, type=str,
                        help='column name')
    parser.add_argument('-c', '--condition', nargs='*', metavar='COL', type=str, default=[],
                        help='column name')
    parser.add_argument('-i', '--input', help='tab-separated file', required=True,
                        type=argparse.FileType('r'),default='-')
    parser.add_argument('-o', '--output', help='output figure', required=True)

    return parser.parse_args()

def read_input(args):

    columns = args.score + args.condition

    data = pd.read_csv(args.input, sep='\t', usecols=columns, header=0)

    if args.normalize == 'log2e':
        data[args.score] = np.log2(math.e) * data[args.score]
    elif args.normalize == 'log10e':
        data[args.score] = np.log10(math.e) * data[args.score]

    return data

def run_ecdf(data, args):

    sns.set(style="ticks")

    condition = args.condition[0]
    score = args.score[0]

    grouped_data = data.groupby(condition)

    fig = plt.figure()
    ax = fig.add_subplot(111)

    for key, subgroup in grouped_data:
        ax = plot_ecdf(ax, sample=subgroup[score], xlabel=score, label=key)

    ax.legend(title=condition, loc='lower right')
#     sns.set_style("ticks")
    sns.despine(offset=5, trim=True)

    # output
    fig.set_tight_layout(True)
    fig.savefig(args.output, format=args.format, dpi=300)
    plt.close('all')

    pass

def plot_ecdf(ax, sample=None, xlabel=None, label=None):
    """Plot empirical cumulative distribution function.
    """

    DOWNSAMPLE = 1e3

    sampling_rate = math.ceil(len(sample)/DOWNSAMPLE)

    x = np.sort(sample)[::sampling_rate]

    sample_size = len(x)

    y = np.arange(sample_size)/sample_size

    ax.step(x, y, label=label)
    ax.set(xlabel=xlabel,
           ylabel=r'cumulative probability $F(n\approx{n:.0e})$'.format(n=sample_size))

    return ax

if __name__ == '__main__':
    args = cli()
    data = read_input(args)
    run_ecdf(data, args)
