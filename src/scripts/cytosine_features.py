#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import math

import pandas as pd

from Bio import SeqIO, SeqUtils
from Bio.Alphabet import IUPAC

# PIVOT = -0.5

def seqrecord(fasta_file, length=None):
    """Get id and sequence of each fasta record.
    """
    records = SeqIO.parse(fasta_file, "fasta", alphabet=IUPAC.ambiguous_dna)
    for rec in records:
        if not length:
            yield {'id': rec.id, 'seq': rec.seq}
        elif len(rec) == length:
            yield {'id': rec.id, 'seq': rec.seq}


def add_features(seqrecord):
    """Add sequence features to seqrecord.
    """

    for rec in seqrecord:
        seq = rec['seq']
        length = len(seq)

        features = {}

        features['id'] = rec['id']
        features['GC content'] = SeqUtils.GC(seq)/100
        features['GC skew'] = 0.0
        features['CpG fraction'] = 0.0
        features['CpHpG fraction'] = 0.0
        # features['CpHpH fraction'] = 0.0

        if features['GC content'] > 0.0:
            features['GC skew'] = SeqUtils.GC_skew(seq, window=length)[0]
            # CpG context
            cpg_pos = SeqUtils.nt_search(str(seq), 'CG')[1:]
            cpg_neg = [loc + 1 for loc in cpg_pos]
            cpg_locations = sorted(cpg_pos + cpg_neg)
            if cpg_locations:
                cpg_count = len(cpg_locations)
                features['CpG fraction'] = (cpg_count/length)/features['GC content']
            # CpHpG context
            chg_pos = SeqUtils.nt_search(str(seq), 'CHG')[1:]
            chg_neg = [loc + 2 for loc in SeqUtils.nt_search(str(seq), 'CDG')[1:]] 
            chg_locations = sorted(chg_pos + chg_neg)
            if chg_locations:
                chg_count = len(chg_locations)
                features['CpHpG fraction'] = (chg_count/length)/features['GC content']
            # CpHpH context
            # - actually should be 1 - CpG - CpHpG, but can be lower due to ambiguous bases in seq
            # - can also be larger due to roundoff error
            # chh_pos = SeqUtils.nt_search(str(seq), 'CHH')[1:]
            # chh_neg = [loc + 2 for loc in SeqUtils.nt_search(str(seq), 'DDG')[1:]] 
            # chh_locations = sorted(chh_pos + chh_neg)
            # if chh_locations:
            #     chh_count = len(chh_locations)
            #     features['CpHpH fraction'] = chh_count#/length)/features['GC content']

                # pivot_loc = (length if math.copysign(1, PIVOT) == -1 else 0) + PIVOT*length
                # cpg_right = len([loc for loc in cpg_locations if loc > pivot_loc])
                # cpg_left = cpg_count - cpg_right
                # features['CpG skew'] = (cpg_right - cpg_left)/cpg_count

        # promoter/TSS-specific features in -100
        # extended TATA consensus, see Yang et al. 2007 Gene
        # tata_ext = (set(SeqUtils.nt_search(str(seq)[-100:], 'HWHWWWWR')[1:])
        #             - set(SeqUtils.nt_search(str(seq)[-100:], 'HTYTTTWR')[1:])
        #             - set(SeqUtils.nt_search(str(seq)[-100:], 'CAYTTTWR')[1:])
        #             - set(SeqUtils.nt_search(str(seq)[-100:], 'MAMAAAAR')[1:])
        #             - set(SeqUtils.nt_search(str(seq)[-100:], 'CTYAAAAR')[1:]))

        # features['TATA-ext count'] = len(tata_ext)
        # features['TATA-can count'] = len(SeqUtils.nt_search(str(seq)[-100:], 'TATAWAWR')[1:])
        # features['INR count'] = len(SeqUtils.nt_search(str(seq)[-100:], 'YYANWYY')[1:])
        # features['DPE count'] = len(SeqUtils.nt_search(str(seq)[-100:], 'RGWCGTG')[1:]) # should be rather downstream of TSS
        # features['BRE count'] = len(SeqUtils.nt_search(str(seq)[-100:], 'SSRCGCC')[1:])

        yield features


def obsexp_CpG(sequence, gc_content=None, length=None):
    """Calculate (observed - expected) number of CpG for sequence.

    The observed count is calculated from the observed frequencies in the given
    sequence. The expected number of CpGs is based on a sequence model that has
    the same GC content as the observed one but where neighbouring bases are
    independent. Note, that this model does not correct for GC skew. Dividing
    the GC content by 2 assumes that p(C|seq) = p(G|seq). The correct null model
    would drop this equality. Also the length of the sequence should matter.

    after: Bird 1980 Nucleic Acids Research
    """
    if not gc_content:
        gc_content = SeqUtils.GC(sequence) / 100
    if not length:
        length = len(sequence)
    observed = sequence.count('CG') + sequence.count('GC')
    expected = round((gc_content / 2)**2 * length)
    return observed - expected


if __name__ == '__main__':
    # parse input arguments
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-f', '--fasta', type=argparse.FileType('r'), default='-', required=True)
    parser.add_argument('-o', '--output', required=True)
    args = parser.parse_args()
    # process
    features = add_features(seqrecord(args.fasta))
    # output dataframe
    digits = 3#{'GC content': 2, 'GC skew': 2, 'CpG content': 2, 'CpG <distance>': 0}
    df = (pd.DataFrame.from_records(features)
            .set_index('id')
            .round(digits))
    df.to_csv(args.output, sep="\t")
