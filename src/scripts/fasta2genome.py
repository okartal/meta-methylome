#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 26

@author: Oender Kartal
"""

import sys

def fasta2genome(fasta, replacement=None):
    '''Calculate size of each contig from fasta file.
    '''

    count = 0
    length = list()
    seq = list()

    for line in fasta:
        if line.startswith(">"):
            name = line.lstrip(">").split()[0]
            seq.append(name)
            if count:
                length.append(str(count))
                count = 0
        else:
            count += len(line.strip())

    length.append(str(count))

    output = ['\t'.join(pair) + '\n' for pair in zip(seq, length)]

    print(*output, sep='', end="")

    pass

if __name__ == "__main__":

    with open(sys.argv[1], "r") as fasta:
        fasta2genome(fasta)
