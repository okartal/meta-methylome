#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
import math
import sys

import numpy as np
import pandas as pd

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import seaborn as sns
sns.set_style("whitegrid")
sns.set_context("paper")

__version__ = "1.0"
__author__ = "Oender Kartal"

def cli():

    parser = argparse.ArgumentParser(prog='plot_pairgrid', description = 'Statistical plots')

    # parser.usage = 'python %(prog)s [-h] [-v] [-b FORMAT] [-n FACTOR] -s COL:NAME -f COL:NAME FILE > OUTPUT'

    parser.add_argument('-v', '--version', action='version',
                        version='%(prog)s {0}'.format(__version__))
    parser.add_argument('-f', '--format', default='pdf',
                        help='output format (default: pdf)')
    parser.add_argument('-n', '--normalize', metavar='FACTOR', type=str,
                        choices=['log2e', 'log10e'],
                        help='normalizing factor for score (just added for convenience)')
    parser.add_argument('-s', '--score', nargs='+', metavar='COL', required=True, type=str,
                        help='column name')
    parser.add_argument('-c', '--condition', nargs='*', metavar='COL', type=str, default=[],
                        help='column name')
    parser.add_argument('-i', '--input', help='tab-separated file', required=True)
    parser.add_argument('-o', '--output', help='output figure', required=True)

    return parser.parse_args()

def read_map(args):

    dtypes = {
        'chrom': 'category',
        'start': np.uint64,
        'end': np.uint64,
        'JS Divergence (bit)': np.float,
        'Methylation Level': np.float,
        'Coverage': np.uint,
        'Context': 'category',
        'Sample Size': np.uint,
        'Feature': 'category',
        'Feature Strand': 'category',
        'Locus ID': 'category'
        }
    # usecols = args.score + args.condition
    usecols = None
    data = pd.read_csv(args.input, sep='\t', usecols=usecols, header=0,
                       dtype=dtypes)

    # quality filtering
    qc = True
    if qc:
        # define criteria for minimum population sample coverage and
        # average read depth
        # NOTE: this should be upstream in a pipeline
        min_sample_coverage = 2/3
        min_read_depth = 5
        
        min_sample_size = math.ceil(min_sample_coverage*data['Sample Size'].max())
        min_coverage = min_read_depth*min_sample_size
        
        data = data[(data['Sample Size'] >= min_sample_size)
                    & (data['Coverage'] >= min_coverage)]

        if data.empty:
            msg = "Dude. Your quality criteria throw away all the data!"
            sys.exit(msg)

    # normalizing scores
    if args.normalize == 'log2e':
        data[args.score] = np.log2(math.e) * data[args.score]
    elif args.normalize == 'log10e':
        data[args.score] = np.log10(math.e) * data[args.score]

    # add categories for scores
    cat = True
    if cat:
        categories = [
            ('Methylation Level', 'Methylation', 3, ['low', 'medium', 'high']),
            ('JS Divergence (bit)', 'Methylation State', 3, ['stable', 'indifferent', 'metastable'])]

        for score, category, bins, labels in categories:
            if score in set(data.columns):
                data[category] = pd.cut(data[score], bins, labels=labels)

    return data

def plot_pairgrid(data, args, hue=None, hue_order=None):
    """Plot interactions between scores in a grid.
    """

    if hue == 'Context':
        hue_order = 'CG CHG CHH'.split()

    g = sns.PairGrid(data, vars=args.score, hue=hue, hue_order=hue_order)
    g.map_upper(plt.scatter, alpha=0.5)
    #g.map_lower(sns.regplot, scatter=False)
    g.map_diag(plt.hist)
    for ax in g.axes.flat:
        plt.setp(ax.get_xticklabels(), rotation=45)
        g.add_legend()
        g.set(alpha=0.5)
        g.set(xlim=(0, 1), ylim=(0, 1))

    # output
    g.savefig(args.output, format=args.format, dpi=300)
    plt.close('all')

    pass

def boxplot(data, args):

    condition = args.condition[0]
    score = args.score[0]

    sns.set(style="ticks", palette="muted", color_codes=True)
    fig = plt.figure()
    ax = fig.add_subplot(111)

    flierprops = dict(markersize=1)
    sns.boxplot(data=data, x=score, y="Feature", hue=condition,
                flierprops=flierprops, order=None)

    ax.set(xlim=(0, 1))
    sns.despine(trim=True, offset=10)

    # output
    fig.set_tight_layout(True)
    fig.savefig(args.output, format=args.format, dpi=300)
    plt.close('all')

    pass

if __name__ == '__main__':
    args = cli()
    data = read_map(args)
    boxplot(data, args)
