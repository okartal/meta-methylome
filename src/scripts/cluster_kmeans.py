#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
from itertools import combinations

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from scipy.stats import kendalltau
from sklearn.cluster import KMeans

sns.set_style("ticks")

def plot_joint(data, prefix=''):
    """
    """

    for x, y in combinations(data.columns, 2):
        g = sns.jointplot(x, y, data=data, kind="hex", stat_func=kendalltau)
        name = (x + '-' + y).replace(' ', '_')
        g.savefig(prefix + '.' + name + '.svg')
        g.savefig(prefix + '.' + name + '.png')

    pass


if __name__ == '__main__':
    # input
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', type=argparse.FileType('r'),
    default='-', required=True)
    parser.add_argument('-p', '--prefix')
    args = parser.parse_args()
    data = pd.read_csv(args.input, sep='\t', index_col='id')
 
    # process
    plot_joint(data, args.prefix)

    # output
