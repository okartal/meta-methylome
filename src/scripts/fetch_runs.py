#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Function to fetch and merge data from ENA.
"""

__author__ = "Önder Kartal"
__version__ = "0.1"

import sys
import argparse

from fetchdb import ena_run

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.prog = 'fetch_runs'
    parser.description = '''A script to fetch run data from ENA.'''
    parser.add_argument('-t', '--taxon', metavar='NAME',
                        default='Arabidopsis thaliana',
                        help="taxon name in quotes (default: %(default)s)"
                        )

    # input
    args = parser.parse_args()

    # process
    data = ena_run(taxon=args.taxon)
    
    # output
    data.to_csv(sys.stdout, sep='\t', encoding='utf-8', index=False)
