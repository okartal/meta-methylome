#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import os
import re
import sys
from math import e as euler

import numpy as np
import pandas
from pybedtools import BedTool

if __name__ == '__main__':

    # parser

    parser = argparse.ArgumentParser()

    parser.prog = 'map_features'
    parser.description = 'Map shannon BED file to GFF.'

    parser.add_argument('--context', choices=['CG', 'CHG', 'CHH'], type=str)
    parser.add_argument('gff')
    parser.add_argument('bed')
    parser.add_argument('out', help='Output file.')

    args = parser.parse_args()

    if os.path.isfile(args.out) and not os.stat(args.out).st_size == 0:
        msg = "\nExecution stops! Output file exists and is not empty!\n"
        sys.exit(msg)

    # input
    gff = BedTool(args.gff)
    bed = BedTool(args.bed)

    if not args.context:
        context_pattern = re.compile('(CG|CpG|CHG|CHH)', re.IGNORECASE)
        context = context_pattern.findall(args.bed)
        if not context:
            msg = ("\nContext is not in filename."
                   " Please change filename or provide --context explicitly.\n")
            sys.exit(msg)
        elif len(set(context)) > 1:
            msg = ("\nContext in filename is ambigious {c}."
                   " Please change filename or provide --context"
                   " explicitly.\n".format(c=context))
            sys.exit(msg)
        else:
            context = context[0]
    else:
        context = args.context

    # intersect and convert to dataframe
    column = [(2, 'feature', str),
              (6, 'feature_strand', str),
              (8, 'AGI', str),
              (9, 'chrom', str),
              (10, 'start', np.uint),
              (11, 'end', np.uint),
              (13, 'JSD (bit)', np.float),
              (14, 'E', np.uint),
              (15, 'C', np.uint),
              (16, 'sample size', np.uint)]

    cols = [x[0] for x in column]
    names = [x[1] for x in column]
    types = dict([x[1:] for x in column])

    regex_agi = re.compile('AT\w{2,3}\d{5,5}', re.IGNORECASE)

    intersection = (gff.intersect(bed, wa=True, wb=True, sorted=True)
                    .to_dataframe(usecols=cols, names=names, dtype=types,
                                  na_values='.', chunksize=100000,
                                  converters={'AGI': lambda x: regex_agi.findall(x)[0]}
                                  )
                    )

    # format output
    newcol = ['methlevel', 'coverage', 'context']
    outcol = names[3:7] + newcol + names[9:10] + names[:3] # weird, but needed for custom order

    for chunk in intersection: # note: this should be parallelizable!
        chunk['context'] = context
        chunk[newcol[0]] = (chunk.E/(chunk.E + chunk.C)).round(2)
        chunk[newcol[1]] = chunk.E + chunk.C
        chunk['JSD (bit)'] = (np.log2(euler)*chunk['JSD (bit)']).round(2)
        need_header = not os.path.isfile(args.out)
        chunk.to_csv(args.out, sep='\t', encoding='utf-8', index=False,
                     columns=outcol, mode='a', header=need_header)
