#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 26

@author: Oender Kartal
"""

import sys
import json

def replace_seqid(infile, replacement=None, col=">", invert=False):
    '''Replace sequence ID in fasta file.
    '''

    if invert:
        replacement = {v: k for k, v in replacement.items()}

    if col == ">":
        fasta = infile
        for line in fasta:
            if line.startswith(">"):
                old_id = line.lstrip(">").split()[0]
                new_id = replacement[old_id]
                newline = line.replace(old_id, new_id, 1)
                print(newline, end="")
            else:
                print(line, end="")
    else:
        table = infile
        for line in table:
            if not line.startswith("#"):
                old_id = line.strip().split()[int(col)-1]
                new_id = replacement[old_id]
                newline = line.replace(old_id, new_id, 1)
                print(newline, end="")
            else:
                print(line, end="")
    pass

if __name__ == "__main__":

    column = sys.argv[3]
    invert = 0

    with open(sys.argv[1], "r") as infile, open(sys.argv[2], "r") as mapfile:
        replace_seqid(infile, json.load(mapfile), column, invert)
