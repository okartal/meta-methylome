#!/bin/bash
# grep the line number of out of order line and remove everything below
for file in leafWT_*t*bed
do
    sort -c -k2,3n $file 2>&1 \
	| grep -o '[0-9]\+' \
	| head -n1 \
	| \
	(
	    read line;
	    head -n `expr $line - 1` $file
	) > ${file}_correct
done
