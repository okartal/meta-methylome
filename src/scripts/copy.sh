#!/bin/bash
#
# Script to copy data from ScienceCloud volume.
#
scp -i ~/.ssh/sciencecloud.pem -pr ubuntu@172.23.176.115:/okartal_meta-methylome/. .
rm -rf lost+found
