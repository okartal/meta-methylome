#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Fetch metadata for a set of samples from an ENA accession.
"""

__author__ = "Önder Kartal"
__version__ = "0.1"

import argparse
import sys

from utils import get_samples
from fetchdb import ena_metadata

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.prog = 'fetch_metadata'
    parser.description = 'Fetch metadata from ENA.'
    parser.add_argument('file', help='tab-delimited table')
    parser.add_argument('attribute', help='attribute to query')

    # input
    args = parser.parse_args()

    # process
    samples = get_samples(infile=args.file, attribute=args.attribute)
    data = ena_metadata(samples=samples, attribute=args.attribute)

    # output
    data.to_csv(sys.stdout, sep='\t', encoding='utf-8', index=False)
