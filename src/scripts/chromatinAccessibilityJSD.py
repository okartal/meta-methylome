import math

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from iobed import tbx_merge
from sklearn.preprocessing import MinMaxScaler


files = ["/home/oender/data/Ath_leaf_DNase.bedgraph.gz", "/home/oender/Insync/genome_data/shannon_files/leafWT.bed.gz"]
labels = ["leaf DNase", "leaf methylation"]
data_columns = [[(3, "DHS-score", np.float)], [(4, "JSD", np.float)]]
regions = [('1', '0', '1000'), ('2', '0', '10000')]
join = 'inner'

scaler = MinMaxScaler()

d = tbx_merge(files, labels=labels, data_columns=data_columns, regions=regions, join=join)

frame = {}

for region, data in zip(regions, d):
    query = '{0}:{1}-{2}'.format(*region)
    df = pd.DataFrame()
    df[['DHS', 'JSD']] = pd.DataFrame(scaler.fit_transform(data), index=data.index.get_level_values(1))
    frame[query] = (df.reindex(df.index.rename('position'))
                    .rolling(window=math.ceil(len(data)/10))
                    .corr()
                    .major_xs('DHS')
                    .stack()
                    .loc['JSD'])
    plt.figure()
    frame[query].plot(title=query)
    plt.show()


