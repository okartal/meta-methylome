DATA = "/home/oender/CloudStation/data/"
SAMPLES = "leafWT rootWT".split()
CHROMS = "1 2 3 4 5 Mt Pt".split()
CONTEXTS = "ALL CG CHG CHH".split()

rule target:
    input:
        expand(DATA + "private/meta-methylome/jsd/{sample}.chr{chrom}.{context}.bedgraph",
               sample=SAMPLES, chrom=CHROMS, context=CONTEXTS),
        expand(DATA + "private/meta-methylome/jsd/{sample}.chr{chrom}.{context}.bw",
               sample=SAMPLES, chrom=CHROMS, context=CONTEXTS)


rule jsd_to_bedg:
    input:
        DATA + "private/meta-methylome/jsd/{sample}.bed.gz"
    output:
        DATA + "private/meta-methylome/jsd/{sample}.chr{chrom}.{context}.bedgraph"
    run:
        if wildcards.context == 'ALL':
            shell("tabix {input} {wildcards.chrom} | cut -f 1-3,5 > {output}")
        else:
            shell("tabix {input} {wildcards.chrom} | grep {wildcards.context} | cut -f 1-3,5 > {output}")

rule bedg_to_bigw:
    input:
        bedg = DATA + "private/meta-methylome/jsd/{sample}.chr{chrom}.{context}.bedgraph",
        chrs = DATA + "public/Arabidopsis_thaliana.TAIR10_noChr.chromSize.txt"
    output:
        DATA + "private/meta-methylome/jsd/{sample}.chr{chrom}.{context}.bw"
    shell:
        "bedGraphToBigWig {input.bedg} {input.chrs} {output}"

rule matrix_jsd_gte:
    input:
        regions = expand(DATA + "private/meta-methylome/features/{feature}.bed", feature=["gene", "transposable_element"])


        scores = expand(DATA + "private/meta-methylome/jsd/{{sample}}.chr{{chrom}}.{context}.bw", context=["CG CHG CHH"])
    output:
        DATA + "private/meta-methylome/plot/matrix.{sample}.chr{chrom}.{feature}.gz" 
    run:
        if wildcards.feature == "gene":
            shell("computeMatrix scale-regions -R {input.regions} -S {input.scores}"
                  " -b 1000 -a 1000 -o {output}")
        elif wildcards.feature in ["exon", "intron"]:
            shell("computeMatrix scale-regions -R {input.regions} -S {input.scores}"
                  " -b 10 -a 10 -o {output}")
            
