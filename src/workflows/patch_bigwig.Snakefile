# Patch sequence IDs of BigWig files
#
# requirements:
#   - bigWigToBedGraph
#   - bedGraphToBigWig
#   - sed
#
# configuration:
#   - change path in CHROMSIZE for file containing the chromosome sizes in output BigWig
#   - change BIGWIG glob pattern if needed
#   - change mappings in SUB
#
# usage:
#   from the working directory containing the BigWig files run
#   > snakemake -jp --snakefile <path-to-this-file>

import glob

CHROMSIZE = "/home/oender/CloudStation/projects/uzh-grossniklaus_2014/data/arabidopsis_thaliana/raw/tair/TAIR10_chromSize.txt"

BIGWIG = glob.glob("*.bw")

SUB = {
    'chr1': '1',
    'chr2': '2',
    'chr3': '3',
    'chr4': '4',
    'chr5': '5',
    'chrC': 'Pt',
    'chrM': 'Mt'
}

sedpattern = "s/{0}/{1}/"

rule all:
    input:
        expand("processed/{bw}", bw=BIGWIG)

rule bigw_to_bedg:
    input:
        "{name}.bw"
    output:
        temp("{name}.bedg")
    shell:
        "bigWigToBedGraph {input} {output}"

rule substitute_seq:
    input:
        "{name}.bedg"
    output:
        temp("{name}.subbedg")
    params:
        pattern = ';'.join([sedpattern.format(regex, replace) for regex, replace in SUB.items()])
    shell:
        "sed '{params.pattern}' {input} > {output}"

rule sort_bedg:
    input:
        "{name}.subbedg"
    output:
        temp("{name}.sorted.subbedg")
    shell:
        "sort -k1,1V -k2,3n {input} > {output}"

rule bedg_to_bigw:
    input:
        bedg = "{name}.sorted.subbedg",
        size = CHROMSIZE
    output:
        "processed/{name}.bw"
    shell:
        "bedGraphToBigWig {input.bedg} {input.size} {output}"
