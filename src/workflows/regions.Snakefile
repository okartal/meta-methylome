# note: when extracting features, you have to be aware of overlapping features
# in the annotation. That is why you need to extract the ID of the feature in
# the GFF annotation column. Otherwise you end up with duplicate names for
# example in the fasta extracted from a bed file. In Araport11 there are
# features that overlap EXACTLY, e.g. genes and antisense RNAs, which makes this
# problem sure to occur!

# SCRIPTS = "~/projects/meta-methylome/scripts/"
SCRIPTS = "~/CloudStation/projects/meta-methylome/scripts/"

features_gff = ["gene", "transposable_element_gene", "exon"]
features_bed = ["promoter", "intron", "intergenic"]

rule end:
    input:
        expand("{feat}.gff", feat=features_gff),
        expand("{feat}.bed", feat=features_bed),
        "promoter.fasta",
        "promoter_features.csv"

rule extract_feature:
    input:
        "Araport11_ref_noChr.gff.gz"
    output:
        "{feature}.gff"
    params:
        feat = "{feature}"
    shell:
        "zcat {input}"
        " | sort -k1,1V -k4,5n"
        " | awk 'BEGIN {{OFS=\"\\t\";}} {{if ($3==\"{params.feat}\") print;}}'"
        " > {output}"

rule extract_promoter:
    input:
        genome = "TAIR10_chromosomes.txt",
        genes = "gene.gff"
    output:
        "promoter.bed"
    params:
        flankL = 1001,
        flankR = 0
    shell:
        "bedtools flank -s -i {input.genes} -g {input.genome}"
        " -l {params.flankL} -r {params.flankR} |"
        " awk 'match($9, /ID=(.+)/, post_id) {{OFS=\"\\t\" ;"
        " split(post_id[1], a, \";\") ; id = a[1] ;"
        " print $1, $4, $5, id, {params.flankL}, $7 }}' "
        " | sort -k1,1V -k2,3n"
        " > {output}"

rule extract_intron:
    input:
        gene = "gene.gff",
        exon = "exon.gff"
    output:
        "intron.bed"
    shell:
        "bedtools subtract -a {input.gene} -b {input.exon} |"
        " awk 'match($9, /ID=(.+)/, post_id) {{OFS=\"\\t\" ;"
        " split(post_id[1], a, \";\") ; id = a[1] ;"
        " print $1, $4, $5, id, $5-$4, $7 }}' "
        " | sort -k1,1V -k2,3n"
        " > {output}"

rule extract_intergenic:
    input:
        genome = "TAIR10_chromosomes.txt",
        gene = "gene.gff"
    output:
        "intergenic.bed"
    shell:
        "bedtools complement -i {input.gene} -g {input.genome}"
        " | sort -k1,1V -k2,3n"
        " > {output}"

rule getfasta_promoter:
    input:
        fasta = "TAIR10_sequence_noChr.fasta",
        bed = "promoter.bed"
    output:
        "promoter.fasta"
    shell:
        "bedtools getfasta -s -name -fi {input.fasta} -bed {input.bed}"
        " > {output}"

rule promoter_features:
    input:
        "promoter.fasta"
    output:
        "promoter_features.csv"
    shell:
        SCRIPTS + "./cytosine_features.py -f {input} -o {output}"