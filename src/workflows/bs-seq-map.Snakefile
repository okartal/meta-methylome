import os

configfile: "config.yaml"

FASTQ_FILES = os.path.join(config["fastq_path"], "{sample}_{mate}.fastq.gz")
GENOME = config["genome_path"]

WC = glob_wildcards(FASTQ_FILES)
SAMPLES = set(WC.sample)
CONTEXT = ["CpG", "CHG", "CHH"]
#MATES = set(WC.mate)

# TODO: maybe put genomeprep as subprocess?
rule end:
    input:
        #expand("alignments/{sample}_markdup.bam", sample=SAMPLES),
        expand(os.path.join(GENOME, "Bisulfite_Genome/{bp}_conversion/BS_{bp}.{n}.bt2"),
               bp=["CT", "GA"], n=range(1,5)),
        expand(os.path.join(GENOME, "Bisulfite_Genome/{bp}_conversion/BS_{bp}.rev.{n}.bt2"),
               bp=["CT", "GA"], n=range(1,3)),
        expand(os.path.join(GENOME, "Bisulfite_Genome/{bp}_conversion/genome_mfa.{bp}_conversion.fa"),
               bp=["CT", "GA"]),
        expand("counts/{sample}_{context}.bedGraph.gz.tbi",
               sample=SAMPLES, context=CONTEXT),
        expand("alignments/{sample}.bam.bai",
               sample=SAMPLES)
#        expand("mbias/{sample}_cg.tab", sample=SAMPLES),
#        expand("mbias/{sample}_chg.tab", sample=SAMPLES),
#        expand("mbias/{sample}_chh.tab", sample=SAMPLES)

rule trim_galore:
    input:
        r1 = "data/{sample}_R1.fastq",
        r2 = "data/{sample}_R2.fastq"
    output:
        v1 = "data_after_qc/{sample}_R1_val_1.fq",
        v2 = "data_after_qc/{sample}_R2_val_2.fq"
    shell:
        "trim_galore --dont_gzip --illumina --paired --fastqc -o $(dirname {output.v1}) "
        "{input.r1} {input.r2}"

rule bismark_prepare:
    input:
        GENOME
    shell:
        "bismark_genome_preparation {input}"

rule bismark_align_pe:
    input:
        r1 = "data_after_qc/{sample}_R1_val_1.fq",
        r2 = "data_after_qc/{sample}_R2_val_2.fq",
        genome = GENOME
    output:
        "alignments/{sample}_pe.sam"
    params:
        bt = "--bowtie2",
        seed = "--seedmms 1",
        folder = "-o alignments/",
        base = "--basename {sample}"
    shell:
        "bismark --sam {params.seed} {params.bt} {params.folder} {params.base} "
        "--genome_folder {input.genome} -1 {input.r1} -2 {input.r2}"

rule samtools_sort:
    input:
        "alignments/{sample}_pe.sam"
    output:
        "alignments/{sample}_sorted.sam"
    params:
        prefix = "-T {sample}",
        out = "-O sam"
    shell:
        "samtools sort {params.prefix} {params.out} {input} > {output}"

rule picard_markdup:
    input:
        "alignments/{sample}_sorted.sam"
    output:
        marked = "alignments/{sample}_markdup.sam",
        metrics = "alignments/{sample}_markdup_metrics.txt"
    shell:
        "picard MarkDuplicates I={input} O={output.marked} M={output.metrics}"

rule samtools_view:
    input:
        "alignments/{sample}_markdup.sam"
    output:
        "alignments/{sample}.bam"
    shell:
        "samtools view -b {input} > {output}"

rule samtools_index:
    input:
        "alignments/{sample}.bam"
    output:
        "alignments/{sample}.bam.bai"
    shell:
        "samtools index {input}"

rule pileometh_mbias_cpg:
    input:
        genome = GENOME + "genome.fa",
        bam = "alignments/{sample}.bam"
    output:
        "mbias/{sample}_CpG.tab"
    shell:
        "PileOMeth mbias --noSVG {input.genome} {input.bam} > {output}"

rule pileometh_mbias_chg:
    input:
        genome = GENOME + "genome.fa",
        bam = "alignments/{sample}.bam"
    output:
        "mbias/{sample}_CHG.tab"
    shell:
        "PileOMeth mbias --CHG --noSVG {input.genome} {input.bam} > {output}"

rule pileometh_mbias_chh:
    input:
        genome = GENOME + "genome.fa",
        bam = "alignments/{sample}.bam"
    output:
        "mbias/{sample}_CHH.tab"
    shell:
        "PileOMeth mbias --CHH --noSVG {input.genome} {input.bam} > {output}"

# TODO: add here a script that segments the mbias to extract inclusion bounds
# as parameters to PileOMeth extract

rule pileometh_extract_cpg:
    input:
        bam = "alignments/{sample}.bam",
        genome = GENOME + "genome.fa"
    output:
        "alignments/{sample}_CpG.bedGraph"
    shell:
        "PileOMeth extract {input.genome} {input.bam}"

rule pileometh_extract_chg:
    input:
        bam = "alignments/{sample}.bam",
        genome = GENOME + "genome.fa"
    output:
        "alignments/{sample}_CHG.bedGraph"
    shell:
        "PileOMeth extract --CHG {input.genome} {input.bam}"

rule pileometh_extract_chh:
    input:
        bam = "alignments/{sample}.bam",
        genome = GENOME + "genome.fa"
    output:
        "alignments/{sample}_CHH.bedGraph"
    shell:
        "PileOMeth extract --CHH {input.genome} {input.bam}"

rule sort_bedgraph:
    # skips the bedgraph header line
    input:
        "alignments/{sample}_{context}.bedGraph"
    output:
        "counts/{sample}_{context}.bedGraph"
    shell:
        "cat {input} | awk 'NR<2{{print $0;next}}{{print $0|"
        " \"sort -k1,1V -k2,2n -k3,3n\"}}' > {output}"

rule bgzip:
    input:
        "counts/{sample}_{context}.bedGraph"
    output:
        "counts/{sample}_{context}.bedGraph.gz"
    shell:
        "bgzip {input}"

rule tabix_bedgraph:
    # takes into account the header line
    # NOTE: add -0 option if the coordinates are zero-based
    input:
        "counts/{sample}_{context}.bedGraph.gz"
    output:
        "counts/{sample}_{context}.bedGraph.gz.tbi"
    shell:
        "tabix -0 -s1 -b2 -e3 -S1 {input}"
