# see also
# https://sequencing.qcfail.com/articles/mispriming-in-pbat-libraries-causes-methylation-bias-and-poor-mapping-efficiencies/
import os

configfile:
    "config.yaml"

wildcard_constraints:
    read = "R1|R2"

READS = os.path.join(config["read_dir"], "{sample}_{read}.fastq.gz")
WC = glob_wildcards(READS)

CONTEXT = ["CpG", "CHG", "CHH"]
MODES = ["pe", "R1", "R2"]
# SAMPLES = set(WC.sample)
SAMPLES = config["samples"]

rule end:
    input:
        expand(config["alignment_dir"] + "{sample}_{mode}_markdup.bam",
               sample=SAMPLES, mode=MODES),
        expand(config["alignment_dir"] + "{sample}_{mode}_markdup.bam.bai",
               sample=SAMPLES, mode=MODES),
        expand(config["alignment_dir"] + "{sample}_{mode}_markdup_stats.txt",
               sample=SAMPLES, mode=MODES),
        expand(config["mbias_dir"] + "{sample}_{mode}_markdup.M-bias.txt",
               sample=SAMPLES, mode=MODES),
        expand(config["mbias_dir"] + "{sample}_{mode}_markdup_splitting_report.txt",
               sample=SAMPLES, mode=MODES)

rule trim_galore:
    input:
        r1 = config["read_dir"] + "{sample}_R1.fastq.gz",
        r2 = config["read_dir"] + "{sample}_R2.fastq.gz"
    output:
        r1 = temp(config["read_trimmed_dir"] + "{sample}_R1_val_1.fq"),
        r2 = temp(config["read_trimmed_dir"] + "{sample}_R2_val_2.fq")
    params:
        # trim random primer length
        clip = 9,
        mode = "--paired"
    shell:
        "trim_galore --dont_gzip --clip_R1 {params.clip} --clip_R2 {params.clip}"
        " {params.mode} -o $(dirname {output.r1}) {input.r1} {input.r2}"

rule bismark_pe:
    input:
        r1 = config["read_trimmed_dir"] + "{sample}_R1_val_1.fq",
        r2 = config["read_trimmed_dir"] + "{sample}_R2_val_2.fq",
        gen = config["genome_dir"]
    output:
        temp(config["alignment_dir"] + "{sample}_pe.bam"),
        temp(config["alignment_dir"] + "{sample}_unmapped_reads_1.fq.gz"),
        temp(config["alignment_dir"] + "{sample}_unmapped_reads_2.fq.gz")
    params:
        aligner = "--bowtie2",
        library = "--non_directional",
        mismatch = "1",
        outdir = config["alignment_dir"],
        name = "{sample}"
    shell:
        "bismark {params.aligner} {params.library} --seedmms {params.mismatch}"
        " -o {params.outdir} --basename {params.name} --unmapped"
        " --genome_folder {input.gen} -1 {input.r1} -2 {input.r2}"

rule bismark_unmapped_R1:
    input:
        read = config["alignment_dir"] + "{sample}_unmapped_reads_1.fq.gz",
        gen = config["genome_dir"]
    output:
        temp(config["alignment_dir"] + "{sample}_R1.bam")
    params:
        aligner = "--bowtie2",
        library = "--pbat",
        mode = "--single_end",
        mismatch = "1",
        outdir = config["alignment_dir"],
        name = "{sample}_R1"
    shell:
        "bismark {params.aligner} {params.library} --seedmms {params.mismatch}"
        " -o {params.outdir} --basename {params.name}"
        " --genome_folder {input.gen} {params.mode} {input.read}"

rule bismark_unmapped_R2:
    input:
        read = config["alignment_dir"] + "{sample}_unmapped_reads_2.fq.gz",
        gen = config["genome_dir"]
    output:
        temp(config["alignment_dir"] + "{sample}_R2.bam")
    params:
        aligner = "--bowtie2",
        library = "--non_directional",
        mode = "--single_end",
        mismatch = "1",
        outdir = config["alignment_dir"],
        name = "{sample}_R2"
    shell:
        "bismark {params.aligner} {params.library} --seedmms {params.mismatch}"
        " -o {params.outdir} --basename {params.name}"
        " --genome_folder {input.gen} {params.mode} {input.read}"

rule bamtools_sort:
    input:
        config["alignment_dir"] + "{sample}_{mode}.bam"
    output:
        temp(config["alignment_dir"] + "{sample}_{mode}.sorted.bam")
    shell:
        "bamtools sort -in {input} -out {output}"

rule picard_markdup:
    input:
        config["alignment_dir"] + "{sample}_{mode}.sorted.bam"
    output:
        marked = config["alignment_dir"] + "{sample}_{mode}_markdup.bam",
        metric = config["alignment_dir"] + "{sample}_{mode}_markdup_metrics.txt"
    shell:
        "picard MarkDuplicates I={input} O={output.marked} M={output.metric}"

rule samtools_index:
    input:
        config["alignment_dir"] + "{sample}_{mode}_markdup.bam"
    output:
        config["alignment_dir"] + "{sample}_{mode}_markdup.bam.bai"
    shell:
        "samtools index {input}"

rule bamtools_stats:
    input:
        config["alignment_dir"] + "{sample}_{mode}_markdup.bam"
    output:
        config["alignment_dir"] + "{sample}_{mode}_markdup_stats.txt"
    shell:
        "bamtools stats -in {input} > {output}"

rule bismark_mbias:
    input:
        config["alignment_dir"] + "{sample}_{mode}_markdup.bam"
    output:
        config["mbias_dir"] + "{sample}_{mode}_markdup.M-bias.txt",
        config["mbias_dir"] + "{sample}_{mode}_markdup_splitting_report.txt"
    params:
        outdir = config["mbias_dir"]
    shell:
        "bismark_methylation_extractor --mbias_only -o {params.outdir} {input}"
