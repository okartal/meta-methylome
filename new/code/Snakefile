import os
import itertools as it

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pybedtools
import seaborn as sns

from matplotlib.ticker import MaxNLocator
from pybedtools import BedTool
from pybedtools.featurefuncs import gff2bed
from scipy import stats

from utils import bedtools, io
from utils.stats import test_mannwhitneyu
from utils.stats import count_mapped_features, dist_mapped_features
from utils.stats import emp_pvalue

configfile: "config.yaml"

features = (
    ["TE"]
    + ["TE_" + f for f in config["data"]["te_superfamilies"]]
    + ["TE_CMT2_targets"]
    + ["TE_RdDM_targets"]
    + ["intergenic-coding"]
    + ["kee10kb", "kee50kb"]
    + [
        "gene_protein-coding-{}kb-flank".format(s)
        for s in config["params"]["feature_gene"]["slop_kb"]
    ]
    + ["gene_transposable-element"]
    + ["gene_imprinted"]
    + [
        "gene_expression-{}-{}".format(tis, exp)
        for tis in config["params"]["feature_gene_expression"]["tissue"]
        for exp in config["params"]["feature_gene_expression"]["expression"]
    ]
    + ["chromatin-state7", "chromatin-state8"]
    + ["promoter_genic_Coreless"]
    + ["promoter_genic_Ypatch"]
    + ["promoter_genic_GA"]
    + ["promoter_genic_TATA"]
    + ["promoter_genic_TATA,Ypatch"]
    + ["promoter_genic_Ypatch,GA"]
    + ["promoter_genic_TATA,GA"]
    + ["promoter_genic_TATA,Ypatch,GA"]
    + ["promoter_genic_CA"]
    + ["promoter_genic_Ypatch,CA"]
    + ["promoter_genic_TATA,CA"]
)

geneset_ind = set()

for key, val in config["params"]["reldist_genes_targetedTEs"].items():
    if val["alias"] == "all":
        geneset_all = set(val["set"])
    else:
        geneset_ind.update(val["set"])
        
geneset_plot = geneset_all.intersection(geneset_ind)

rule all:
    input:
        expand(
            "feature_TE_{signal}-profile_{etype}_{gtype}_{src}.mat.gz",
            signal=config["data"]["diversity"],
            etype=config["data"]["genome"]["ecotype"],
            gtype=config["data"]["genome"]["genotype"],
            src=config["data"]["source"]
        ),
        expand(
            "fig/plot-profile-{signal}_feature_TE_{etype}_{gtype}_{src}.svg",
            signal=config["data"]["diversity"],
            etype=config["data"]["genome"]["ecotype"],
            gtype=config["data"]["genome"]["genotype"],
            src=config["data"]["source"],
        ),
        expand(
            "plot-profile-{signal}_feature_TE_{etype}_{gtype}_{src}.dat",
            signal=config["data"]["diversity"],
            etype=config["data"]["genome"]["ecotype"],
            gtype=config["data"]["genome"]["genotype"],
            src=config["data"]["source"],
        ),
        expand(
            "probability-ctype_{feature}_{ctx}_{etype}_{gtype}_{src}.bed",
            feature=features,
            ctx=config["data"]["cytosine"]["context"],
            etype=config["data"]["genome"]["ecotype"],
            gtype=config["data"]["genome"]["genotype"],
            src=config["data"]["source"]
        ),
        expand(
            "spearman-{ctype}_{ctx}_{etype}_{gtype}_{src}.csv",
            ctype=["all"] + config["data"]["cytosine"]["type"],
            ctx=config["data"]["cytosine"]["context"],
            etype=config["data"]["genome"]["ecotype"],
            gtype=config["data"]["genome"]["genotype"],
            src=config["data"]["source"]
        ),
        "plot-spearman_jsdmet.csv",
        "fig/plot-spearman_jsdmet.svg",
        expand(
            "fig/plot-ctype_TE_{ctx}_{etype}_{gtype}_{src}.svg",
            ctx=config["data"]["cytosine"]["context"],
            etype=config["data"]["genome"]["ecotype"],
            gtype=config["data"]["genome"]["genotype"],
            src=config["data"]["source"]
        ),
        expand(
            "fig/plot-phaseplane_{ctx}_{etype}_{gtype}_{src}.svg",
            ctx=config["data"]["cytosine"]["context"],
            etype=config["data"]["genome"]["ecotype"],
            gtype=config["data"]["genome"]["genotype"],
            src=config["data"]["source"]
        ),
        "count-ctype_genome.csv",
        expand(
            "tests-mannwhitneyu_jsd-between-context_{etype}_{gtype}_{src}.csv",
            etype=config["data"]["genome"]["ecotype"],
            gtype=config["data"]["genome"]["genotype"],
            src=config["data"]["source"]
        ),
        expand(
            "stats_{etype}_{gtype}_{src}.csv",
            etype=config["data"]["genome"]["ecotype"],
            gtype=config["data"]["genome"]["genotype"],
            src=config["data"]["source"]
        ),
        expand(
            "fig/plot-profile-{sig}_chromstate_{etype}_{gtype}_{src}.svg",
            sig=config["data"]["diversity"],
            etype=config["data"]["genome"]["ecotype"],
            gtype=config["data"]["genome"]["genotype"],
            src=config["data"]["source"]
        ),
        expand(
            "plot-profile-{sig}_chromstate_{etype}_{gtype}_{src}.dat",
            sig=config["data"]["diversity"],
            etype=config["data"]["genome"]["ecotype"],
            gtype=config["data"]["genome"]["genotype"],
            src=config["data"]["source"]
        ),
        expand(
            "fig/gviz_{gene}_{etype}_{gtype}_{src}.svg",
            gene=config["params"]["plot_gviz"]["genes"].keys(),
            etype=config["data"]["genome"]["ecotype"],
            gtype=config["data"]["genome"]["genotype"],
            src=config["data"]["source"]
        ),
        expand(
            "coverage-{size}bp_{feature}.bed",
            size=config["params"]["genomebins"],
            feature=features
        ),
        expand(
            "top-msc_gene_protein-coding-{slop}kb-flank_{etype}_{gtype}.bed",
            slop=config["params"]["feature_gene"]["slop_kb"],
            etype=config["data"]["genome"]["ecotype"],
            gtype=config["data"]["genome"]["genotype"]
        ),
        expand(
            "fig/plot-upset-source_{top}_{ctx}_{etype}_{gtype}.svg",
            ctx=config["params"]["plot_upset"]["context"],
            top=config["params"]["plot_upset"]["top"],
            etype=config["data"]["genome"]["ecotype"],
            gtype=config["data"]["genome"]["genotype"]
        ),
        expand(
            "genes-unique-source_MSC-{top}_{ctx}_{etype}_{gtype}.csv",
            ctx=config["params"]["plot_upset"]["context"],
            top=config["params"]["plot_upset"]["top"],
            etype=config["data"]["genome"]["ecotype"],
            gtype=config["data"]["genome"]["genotype"]
        ),
        expand(
            "genes-unique_{src}_MSC-{top}_{ctx}_{etype}_{gtype}.txt",
            src=[s.replace(" ", "-") for s in config["params"]["plot_upset"]["sets"]],
            ctx=config["params"]["plot_upset"]["context"],
            top=config["params"]["plot_upset"]["top"],
            etype=config["data"]["genome"]["ecotype"],
            gtype=config["data"]["genome"]["genotype"]
        ),
        expand(
            "fig/plot-upset-context_{top}_{src}_{etype}_{gtype}.svg",
            src=[s.replace(" ", "-") for s in config["params"]["plot_upset"]["sets"]],
            top=config["params"]["plot_upset"]["top"],
            etype=config["data"]["genome"]["ecotype"],
            gtype=config["data"]["genome"]["genotype"]
        ),
        # expand(
        #     "tests/fisher_{top}-metastable-genes_{feat}_{ctx}_{etype}_{gtype}_{src}.txt",
        #     top=config["params"]["plot_upset"]["top"],
        #     feat=features,
        #     ctx=config["params"]["plot_upset"]["context"],
        #     etype=config["data"]["genome"]["ecotype"],
        #     gtype=config["data"]["genome"]["genotype"],
        #     src=config["data"]["source"]
        # ),
        # "fig/tests-fisher_overlap-metastable-genes.csv",
        expand(
            "feature_gene_expression-{tissue}-{expression}.bed",
            tissue=config["params"]["feature_gene_expression"]["tissue"],
            expression=config["params"]["feature_gene_expression"]["expression"]
        ),
        expand(
            "profile_gene_expression-{tissue}_{sig}_{etype}_{gtype}_{source}.mat.gz",
            tissue=config["params"]["feature_gene_expression"]["tissue"],
            sig=config["data"]["diversity"],
            etype=config["data"]["genome"]["ecotype"],
            gtype=config["data"]["genome"]["genotype"],
            source=config["data"]["source"]
        ),
        expand(
            "fig/plot-profile-{sig}_gene-expression-{tissue}_{etype}_{gtype}_{source}.svg",
            tissue=config["params"]["feature_gene_expression"]["tissue"],
            sig=config["data"]["diversity"],
            etype=config["data"]["genome"]["ecotype"],
            gtype=config["data"]["genome"]["genotype"],
            source=config["data"]["source"]
        ),
        expand(
            "plot-profile-{sig}_gene-expression-{tissue}_{etype}_{gtype}_{source}.dat",
            tissue=config["params"]["feature_gene_expression"]["tissue"],
            sig=config["data"]["diversity"],
            etype=config["data"]["genome"]["ecotype"],
            gtype=config["data"]["genome"]["genotype"],
            source=config["data"]["source"]
        ),
        expand(
            "feature_gene_nearby-chromstate{num}.bed",
            num=range(1, 10)
        ),
        expand(
            "HiC-PCA_{chromosome_arm}.bedg",
            chromosome_arm=config["params"]["hicdata2bedg"]
        ),
        expand(
            "{signal}_{context}_{etype}_{gtype}_{source}.bedg",
            context=config["data"]["cytosine"]["context"],
            signal=config["data"]["diversity"],
            etype=config["data"]["genome"]["ecotype"],
            gtype=config["data"]["genome"]["genotype"],
            source=config["data"]["source"]            
        ),
        expand(
            "mean-{signal}-{size}bp_{context}_{etype}_{gtype}_{source}.bed",
            context=config["data"]["cytosine"]["context"],
            signal=config["data"]["diversity"],
            etype=config["data"]["genome"]["ecotype"],
            gtype=config["data"]["genome"]["genotype"],
            source=config["data"]["source"],
            size=config["params"]["genomebins"]
        ),
        "HiC-PCA.bedg",
        "intersection_HiC-PCA_signal-mean_50000bp.bed",
        "correlation_HiC-PCA_signal-mean_50000bp.csv",
        expand(
            "feature_gene_protein-coding-{slop}kb-flank_{pct}-msc_{ctx}_{src}_{etype}_{gtype}.bed",
            pct=config["params"]["plot_upset"]["top"],
            ctx=config["params"]["plot_upset"]["context"],
            src=[s.replace(" ", "-") for s in config["params"]["plot_upset"]["sets"]],
            slop=config["params"]["feature_gene"]["slop_kb"],
            etype=config["data"]["genome"]["ecotype"],
            gtype=config["data"]["genome"]["genotype"]
        ),
        expand(
            "genomebin_{size}bp.bed",
            size=config["params"]["genomebins"]
        ),
        expand(
            "coverage-{size}bp_gene_protein-coding-0kb-flank_{pct}-msc_{ctx}_{src}_{etype}_{gtype}.bed",
            size=config["params"]["genomebins"],
            slop=config["params"]["feature_gene"]["slop_kb"],
            pct=config["params"]["plot_upset"]["top"],
            ctx=config["params"]["plot_upset"]["context"],
            src=[s.replace(" ", "-") for s in config["params"]["plot_upset"]["sets"]],
            etype=config["data"]["genome"]["ecotype"],
            gtype=config["data"]["genome"]["genotype"]            
        ),
        expand(
            "fraction-{size}bp_gene_protein-coding-0kb-flank_{pct}-msc_{ctx}_{src}_{etype}_{gtype}.bedg",
            size=config["params"]["genomebins"],
            slop=config["params"]["feature_gene"]["slop_kb"],
            pct=config["params"]["plot_upset"]["top"],
            ctx=config["params"]["plot_upset"]["context"],
            src=[s.replace(" ", "-") for s in config["params"]["plot_upset"]["sets"]],
            etype=config["data"]["genome"]["ecotype"],
            gtype=config["data"]["genome"]["genotype"]            
        ),
        expand(
            "fraction-{size}bp_TE_{family}.bedg",
            size=config["params"]["genomebins"],
            family=config["data"]["te_superfamilies"] + ["CMT2_targets"] + ["RdDM_targets"],
        ),
        expand(
            "ecdf_{sig}_{ctx}_Col-0_wt_{src}.csv",
            sig=config["data"]["diversity"],
            ctx=config["data"]["cytosine"]["context"],
            src=config["data"]["source"],
         ),
        "ecdf_Col-0_wt.csv",
        "fig/plot-ecdf_Col-0_wt.svg",
        expand(
            "plot-clustermap-fraction-{size}bp.csv",
            size=config["params"]["genomebins"],
        ),
        expand(
            "fig/plot-clustermap-fraction-{size}bp.svg",
            size=config["params"]["genomebins"],
        ),
        "feature_gene_protein-coding-0kb-flank_not-overlapping-TE-genes.bed",
        "feature_gene_protein-coding-0kb-flank_overlapping-TE-genes.bed",
        expand(
            "genes_{sig}_{ctx}.mat.gz",
            sig=config["data"]["diversity"],
            ctx=config["data"]["cytosine"]["context"],
        ),            
        expand(
            "fig/plot-profile-{sig}_genes_{ctx}.svg",
            sig=config["data"]["diversity"],
            ctx=config["data"]["cytosine"]["context"],
        ),
        expand(
            "div-feature_{feature}_{ctx}_{etype}_{gtype}_{src}.bed",
            feature=["TE"],
            ctx=config["data"]["cytosine"]["context"],
            etype=config["data"]["genome"]["ecotype"],
            gtype=config["data"]["genome"]["genotype"],
            src=config["data"]["source"],
        ),
        expand(
            "fig/plot-phaseplane_{feature}_{ctx}_{etype}_{gtype}_{src}.svg",
            feature=features,
            ctx=config["data"]["cytosine"]["context"],
            etype=config["data"]["genome"]["ecotype"],
            gtype=config["data"]["genome"]["genotype"],
            src=config["data"]["source"],
        ),
        expand(
            "te_targets_{sig}_{src}.mat.gz",
            sig=config["data"]["diversity"],
            src=config["data"]["source"],
        ),
        expand(
            "fig/plot-profile-{sig}_{src}_feature_te_targets.svg",
            sig=config["data"]["diversity"],
            src=config["data"]["source"],
        ),
        expand(
            "coverage-{size}bp_chromstate{num}.bed",
            size=config["params"]["genomebins"],
            num=range(1, 10),
        ),
        expand(
            "fraction-{size}bp_chromstate{num}.bedg",
            size=config["params"]["genomebins"],
            num=range(1, 10),
        ),
        expand(
            "plot-clustermap-mean-{sig}-{bin}bp_Col-0_wt.csv",
            sig=config["data"]["diversity"],
            bin=config["params"]["genomebins"],
        ),
        expand(
            "fig/plot-clustermap-mean-{sig}-{bin}bp_Col-0_wt.svg",
            sig=config["data"]["diversity"],
            bin=config["params"]["genomebins"],
        ),
        expand(
            "genes-nearby-TEs_{gene}_{path}-targets.bed",
            gene=config["params"]["genes_nearby_targetedTEs"]["candidates"],
            path=config["params"]["genes_nearby_targetedTEs"]["targets"],
        ),
        expand(
            "genes-nearby-TEs_random-{size}_draws-{draws}_{path}-targets_{out}.csv",
            out=["count", "TEdist"],
            path=config["params"]["genes_nearby_targetedTEs"]["targets"],
            size=list(config["params"]["genes_nearby_targetedTEs"]["randomized"]["size"].values()),
            draws=config["params"]["genes_nearby_targetedTEs"]["randomized"]["draws"],
        ),
        "reldist_gene_TE.csv",
        expand(
            "fig/plot-reldist_gene_TE_{geneset}.svg",
            geneset=geneset_plot,
        ),
        "TE-superfamilies_for_targets.csv",
        "fig/plot-catplot_TE-superfamilies_for_targets.svg",
        "fig/plot-distributions_HiC-domain.svg",
        "HiC-domains_random_mean.csv",
        "HiC-domains_observed_mean.csv",
        expand(
            "fig/plot-density_genes-nearby-TEs_{gene}_count.svg",
            gene=config["params"]["genes_nearby_targetedTEs"]["randomized"]["size"].keys(),
        ),
        expand(
            "fig/plot-density_HiC-domains_{sig}.svg",
            sig=["met", "jsd"],
        ),
        "tab-pvalues_HiC-domains_mean.csv",
        "tab-pvalues_HiC-domains_mean.tex",
        ###
        # I made the following files by executing the corresponding
        # rules by hand since DAG building was very slow for unknown
        # reasons (see bw_mean and bw_log2ratio in deeptools.smk)
        ###
        # expand(
        #     "{sample}.bw",
        #     sample=config["data"]["bigwig"]["mean"],
        # ),
        # expand(
        #     "{sample}_{signal}-{background}_log2ratio.bw",
        #     sample=config["data"]["bigwig"]["log2ratio"]["zhao2019"]["samples"],
        #     signal=config["data"]["bigwig"]["log2ratio"]["zhao2019"]["signal"],
        #     background=config["data"]["bigwig"]["log2ratio"]["zhao2019"]["background"],
        # ),
        expand(
            "bwsummary_{sig}_{dat}_50kb.npz",
            sig=config["data"]["diversity"],
            dat=["accessibility", "histones"],
        ),
        expand(
            "fig/bwsummary_{sig}_{dat}_50kb.svg",
            sig=config["data"]["diversity"],
            dat=["accessibility", "histones"],
        ),
        "stats_Col-0_wt.tex",
        "proportion-ctype_genome.csv",
        "proportion-ctype_genome.tex",
        "matrix-chromstate-histone_H3K27ac.gz",
        "matrix-chromstate-histone_H3K9me2.gz",
        "fig/plot-profile-histone_H3K27ac.svg",
        "fig/plot-profile-histone_H3K9me2.svg",
        "matrix-TEs-histone_H3K9me2.gz",
        "fig/plot-profile-TEs-histone_H3K9me2.svg",

include: "rules/features.smk"
include: "rules/deeptools.smk"
include: "rules/bedtools.smk"
include: "rules/correlation.smk"
include: "rules/plots.smk"
include: "rules/count_ctype.smk"
include: "rules/top_msc_genes.smk"
include: "rules/stats.smk"
# include: "rules/bedtools_fisher.smk"
include: "rules/hicdata2bedg.smk"
include: "rules/bw_to_bedg_sig.smk"
include: "rules/bedtools_map_jsd_genomebin.smk"
include: "rules/cat_hic.smk"
include: "rules/bedtools_intersect_hic_sig.smk"
include: "rules/correlation_hic_sig.smk"
include: "rules/top_msc_genes_bed.smk"
include: "rules/top_msc_genes_fraction.smk"
include: "rules/transposon_fraction.smk"
include: "rules/plot_clustermap_signal.smk"
include: "rules/genes_nearby_features.smk"
include: "rules/hic_mean_domains.smk"
