rule feature_gene_expression:
    input:
        exp="../primary/ExpressionAnglerOutput_{tissue}_{expression}.txt",
        bed="feature_gene_protein-coding-0kb-flank.bed"
    output:
        bed="feature_gene_expression-{tissue}-{expression}.bed"
    run:
        all_genes = BedTool(input.bed)

        score = {gene: score for gene, score in
                 io.get_expressed_genes(input.exp)}

        (all_genes
         .filter(lambda f: f[3] in score.keys())
         .each(bedtools.update_scores, score)
         .saveas(output.bed))
