rule plot_profile_te_superfamilies:
    input:
        "feature_TE_{signal}-profile_Col-0_wt_{source}.mat.gz"
    output:
        fig="fig/plot-profile-{signal}_feature_TE_{etype}_{gtype}_{source}.svg",
        dat="plot-profile-{signal}_feature_TE_{etype}_{gtype}_{source}.dat"
    params:
        default=config["params"]["plot_profile_te_superfamilies"],
        regions=config["data"]["te_superfamilies"],
    shell:
        "plotProfile {params.default} --regionsLabel {params.regions} -m {input} -out {output.fig}"
        " --outFileNameData {output.dat} --samplesLabel CG CHG CHH"
        
rule plot_cytotype_spearman:
    input:
        csv="plot-spearman_jsdmet.csv"
    output:
        svg="fig/plot-spearman_jsdmet.svg"
    params:
        pvalue = config["params"]["plot_cytotype_spearman"]["pvalue"],
        colormap = config["params"]["colormap"]["cytotype"]
    run:
        sns.set(style="whitegrid")
        sns.set_context("talk")

        data = pd.read_csv(input.csv, header=0)

        # filtering
        data = data[data["p-value"] < params.pvalue]
        data = data[data["cytosine-type"] != "all"]

        # get correct color palette
        data["color"] = [params.colormap[i][j] for i, j in data[["context", "cytosine-type"]].values]
        data["type-by-context"] = data["context"].str.cat(data["cytosine-type"], "-")
        palette = dict(zip(data["type-by-context"], data["color"]))
        
        # change context name for consistency
        data["context"] = data["context"].replace({"CpG": "CG"})
        
        g = sns.swarmplot(
            x="context", y="spearmanr", hue="type-by-context",
            palette=palette, data=data, edgecolor="black",
            linewidth=1, size=5)

        g.get_legend().remove()
  
        fig = g.get_figure()
        fig.set_tight_layout(True)
        fig.savefig(output.svg)

rule plot_cytotype_te_superfamilies:
    """
    TODO: xlim, tightbox
    """
    input:
        bed=["probability-ctype_feature_TE_{feat}_{{ctx}}_{{etype}}_{{gtype}}_{{src}}.bed".format(feat=f) for f in config["data"]["te_superfamilies"]]
    output:
        svg="fig/plot-ctype_feature_TE_{ctx}_{etype}_{gtype}_{src}.svg"
    params:
        groups=config["data"]["te_superfamilies"],
        columns=config["params"]["plot_cytotype_te_superfamilies"]["columns"],
        usecols=config["params"]["plot_cytotype_te_superfamilies"]["usecols"],
        palette=lambda wildcards: config["params"]["colormap"]["cytotype"][wildcards.ctx],
        minsize=config["params"]["plot_cytotype_te_superfamilies"]["minimum group size"]
    run:
        data = pd.concat((pd.read_csv(f, header=None,
                                      names=params.columns, sep="\t",
                                      usecols=params.usecols)[params.usecols]
                          for f in input.bed),
                         keys=params.groups,
                         names=["TE superfamily"])

        # filter out groups with too few representatives
        groupsize = data.groupby(level="TE superfamily").size()
        valid_groups = groupsize.index[groupsize >= params.minsize]
        data = data[data.index.get_level_values(0).isin(valid_groups)]

        dm = data.mean(level="TE superfamily")

        colors = [params.palette[i] for i in params.usecols]

        dm.sort_values(by=["MSC"]).plot(kind="barh", stacked=True, color=colors)

        plt.savefig(output.svg)
        
rule plot_phaseplane:
    input:
        "div_{ctx}_{etype}_{gtype}_{src}.bed.gz"
    output:
        "fig/plot-phaseplane_{ctx}_{etype}_{gtype}_{src}.svg"
    params:
        color = lambda wildcards: config["params"]["colormap"]["cytotype"][wildcards.ctx]["MSC"]
    shell:
        "zcat {input} | plot_phaseplane.py \'{params.color}\' {output}"

# rule plot_correlation:
#     # obsolete, use seaborn clustermap
#     input:
#         npz="{signal}_10000bp_{etype}_{gtype}_specific-summary.npz"
#     output:
#         svg="fig/plot-correlation_{signal}_10000bp_{etype}_{gtype}.svg",
#         dat="plot-correlation_{signal}_10000bp_{etype}_{gtype}.dat"
#     params:
#         config["params"]["plot_correlation"]
#     shell:
#         "plotCorrelation {params} --corData {input.npz} --plotFile {output.svg}"
#         " --outFileCorMatrix {output.dat}" 

rule plot_profile_chromstate:
    input:
        "matrix-chromstate-{sig}_{etype}_{gtype}_{src}.gz"
    output:
        fig="fig/plot-profile-{sig}_chromstate_{etype}_{gtype}_{src}.svg",
        dat="plot-profile-{sig}_chromstate_{etype}_{gtype}_{src}.dat"
    params:
        config["params"]["plot_profile_chromstate"]
    shell:
        "plotProfile --matrixFile {input}"
        " --outFileName {output.fig} --outFileNameData {output.dat}"
        " {params} --samplesLabel CG CHG CHH "

rule plot_gviz:
    """Plot genome tracks centered on a gene.
    """
    input:
        jsd = ["jsd_{ctx}_{{etype}}_{{gtype}}_{{src}}.bw".format(ctx=c) for c in config["data"]["cytosine"]["context"]],
        met = ["met_{ctx}_{{etype}}_{{gtype}}_{{src}}.bw".format(ctx=c) for c in config["data"]["cytosine"]["context"]],
        ann = ["transposable_element.gff"]
    output:
        fig = "fig/gviz_{gene}_{etype}_{gtype}_{src}.svg"
    params:
        gene = lambda wildcards: config["params"]["plot_gviz"]["genes"][wildcards.gene],
        flank = 6000,
        groups = "CG CHG CHH".split(),
        annotation = ["transposon"],
        plot = ["axis", "gene", "transposon", "JSD", "MET CG", "MET CHG", "MET CHH"]
    script:
        "../../../src/scripts/plot_gviz.R"

rule plot_upset_source:
    input:
        top="top-msc_gene_protein-coding_{etype}_{gtype}.bed"
    output:
        fig="fig/plot-upset-source_{top}_{ctx}_{etype}_{gtype}.svg",
        dat="genes-unique-source_MSC-{top}_{ctx}_{etype}_{gtype}.csv"
    params:
        ctx=lambda wildcards: wildcards.ctx,
        top=lambda wildcards: wildcards.top,
        sets=config["params"]["plot_upset"]["sets"],
        color=lambda wildcards: config["params"]["plot_upset"]["colormap"][wildcards.ctx]
    script:
        "../../code/scripts/plot_upset_intersect-source.R"
        
rule plot_upset_context:
    input:
        top="top-msc_gene_protein-coding_{etype}_{gtype}.bed"
    output:
        fig="fig/plot-upset-context_{top}_{src}_{etype}_{gtype}.svg"
    params:
        src=lambda wildcards: wildcards.src.replace("-", " "),
        top=lambda wildcards: wildcards.top,
        colormap=config["params"]["plot_upset"]["colormap"]
    script:
        "../../code/scripts/plot_upset_intersect-context.R"
        
rule plot_profile_expression:
    input:
        "profile_gene_expression-{tissue}_{sig}_{etype}_{gtype}_{source}.mat.gz"
    output:
        fig="fig/plot-profile-{sig}_gene-expression-{tissue}_{etype}_{gtype}_{source}.svg",
        dat="plot-profile-{sig}_gene-expression-{tissue}_{etype}_{gtype}_{source}.dat",
    params:
        config["params"]["plot_profile_expression"]
    shell:
        "plotHeatmap --matrixFile {input} --outFileName {output.fig}"
        " --outFileSortedRegions {output.dat} {params}"

rule plot_hic_correlation:
    input:
        csv="correlation_HiC-PCA_signal-mean_50000bp.csv"
    output:
        fig="fig/plot-correlation_HiC-PCA_signal-mean_50000bp.svg"
    run:
        sns.set_context("talk")
        
        data = pd.read_csv(input.csv, header=0)

        g = sns.relplot(x="spearmanr", y="-log10(pvalue)",
                        col="source", col_wrap=3, style="signal", hue="context",
                        alpha=0.5, aspect=1, data=data)
        
        g.savefig(output.fig)

rule make_ecdf_each:
    input:
        bedg="{sig}_{ctx}_Col-0_wt_{src}.bedg"
    output:
        csv="ecdf_{sig}_{ctx}_Col-0_wt_{src}.csv"
    params:
        ctx=lambda wildcards: wildcards.ctx.replace("p", ""),
        sig=lambda wildcards: wildcards.sig.upper(),
    shell:
        "cut -f4 {input.bedg}"
        " | sort -n"
        " | ./../../code/scripts/ecdf.py"
        " | awk '{{print $0\",{params.sig},{params.ctx},{wildcards.src}\"}}'"
        "> {output.csv}"
        
rule make_ecdf_table:
    input:
        expand(
            "ecdf_{sig}_{ctx}_Col-0_wt_{src}.csv",
            sig=config["data"]["diversity"],
            ctx=config["data"]["cytosine"]["context"],
            src=config["data"]["source"],
        ),
    output:
        "ecdf_Col-0_wt.csv"
    shell:
        "echo 'value,frequency,quantity,context,source' > {output.csv};"
        "cat {input} >> {output}"
  
rule plot_ecdf:
    """
    Lineplot of empirical cumulative distribution functions.
    """
    input:
        csv="ecdf_Col-0_wt.csv"
    output:
        fig="fig/plot-ecdf_Col-0_wt.svg"
    params:
        palette=config["params"]["colormap"]["context"]
    run:
        sns.set_context("paper", font_scale=1.4)
        
        data = pd.read_csv(input.csv, header=0)
        
        g = sns.lineplot(x="value", y="frequency",hue="context",
                         style="quantity", palette=params.palette,
                         legend="brief", data=data)

        g.set_xlim(-0.1, 1.1)
        g.set_ylim(-0.1, 1.1)

        g.figure.savefig(output.fig)

rule data_clustermap_msg:
    input:
        "fraction-{size}bp_{feature}.bedg"
    output:
        temp("fraction-{size}bp_{feature}.dat")
    shell:
        "echo '{wildcards.feature}' > {output};"
        "cut -f4 {input} >> {output}"

clustermap_features = (
    ["TE_" + f for f in config["data"]["te_superfamilies"] + ["CMT2_targets"] + ["RdDM_targets"]]
    + [
        "gene_protein-coding-0kb-flank_top5pct-msc_{}_{}_Col-0_wt".format(ctx, src)
        for ctx in config["params"]["plot_upset"]["context"]
        for src in [s.replace(" ", "-") for s in config["params"]["plot_upset"]["sets"]]
    ]
    + ["chromstate{}".format(n) for n in range(1, 10)] 
)

rule paste_clustermap_msg:
    input:
        expand(
            "fraction-{{size}}bp_{feature}.dat",
            feature=clustermap_features
        ),
    output:
        "plot-clustermap-fraction-{size}bp.csv"
    shell:
        "paste {input} > {output}"

rule plot_clustermap_msg:
    input:
        csv="plot-clustermap-fraction-{size}bp.csv"
    output:
        fig="fig/plot-clustermap-fraction-{size}bp.svg"
    run:
        sns.set_context("paper")
        
        data = pd.read_csv(input.csv, header=0, sep="\t", na_values=["."])
        
        data.columns = [
            (c
             .replace("gene_protein-coding-0kb-flank_top5pct-msc_", "MSG ")
             .replace("_Col-0_wt", "")
             .replace("chromstate", "Chromatin State ")
             .replace("_", " ").replace("-", " ")
            ) for c in data.columns
        ]
        
        g = sns.clustermap(data.corr(method="spearman"), center=0, cmap="vlag",
                           linewidths=0.75, figsize=(13, 13))

        g.savefig(output.fig)

rule plot_profile_genes:
    input:
        "genes_{sig}_{ctx}.mat.gz"
    output:
        fig="fig/plot-profile-{sig}_genes_{ctx}.svg",
        dat="plot-profile-{sig}_genes_{sig}_{ctx}.dat",
    params:
        default=config["params"]["plot_profile_genes"],
        samples=config["data"]["source"],
    shell:
        "plotProfile {params.default} --samplesLabel {params.samples} --matrixFile {input}"
        " --outFileName {output.fig} --outFileNameData {output.dat}"

rule plot_phaseplane_features:
    input:
        "div-feature_{feature}_{ctx}_{etype}_{gtype}_{src}.bed"
    output:
        "fig/plot-phaseplane_{feature}_{ctx}_{etype}_{gtype}_{src}.svg"
    params:
        color = lambda wildcards: config["params"]["colormap"]["cytotype"][wildcards.ctx]["MSC"]
    shell:
        "cat {input} | ../../../src/scripts/plot_phaseplane.py \'{params.color}\' {output}"
        
rule plot_profile_te_targets:
    input:
        "te_targets_{sig}_{src}.mat.gz"
    output:
        fig="fig/plot-profile-{sig}_{src}_feature_te_targets.svg",
        dat="plot-profile-{sig}_{src}_feature_te_targets.dat",
    params:
        default=config["params"]["plot_profile_te_targets"],
    shell:
        "plotProfile {params.default} --matrixFile {input}"
        " --outFileName {output.fig} --outFileNameData {output.dat}"

rule plot_reldist:
    input:
        csv="reldist_gene_TE.csv"
    output:
        fig="fig/plot-reldist_gene_TE_{geneset}.svg"
    params:
        conf=config["params"]["reldist_genes_targetedTEs"]
    run:
        sns.set_context("talk")
        
        reldist = pd.read_csv(input.csv, header=0, sep="\t", usecols=[0, 3, 4, 5])
        
        geneset = [key for key, val in params.conf.items() if wildcards.geneset in val["set"]]
        
        alias = {key: val["alias"] for key, val in params.conf.items()}

        palette = dict()
        default_palette = it.cycle(sns.color_palette("husl", 9))
        
        for key, val in params.conf.items():
            if val["color"]:
                palette[val["alias"]] = val["color"]
            else:
                palette[val["alias"]] = next(default_palette)

        reldist = reldist[reldist.gene.isin(geneset)]
        
        reldist.rename(columns={"reldist": "relative distance",
                                "fraction": "frequency"},
                       inplace=True)

        reldist.gene.replace(alias, inplace=True)

        g = sns.relplot(x="relative distance", y="frequency",
                        hue="gene", row="TE", palette=palette,
                        height=5, aspect=1,
                        facet_kws=dict(sharex=True), kind="line",
                        legend=False, alpha=0.7, data=reldist)

        g.savefig(output.fig)
        
rule plot_TE_target_superfamilies:
    input:
        csv="TE-superfamilies_for_targets.csv"
    output:
        fig="fig/plot-catplot_TE-superfamilies_for_targets.svg"
    run:
        data = pd.read_csv(input.csv, header=0, index_col=["pathway",
                                                           "TE superfamily"])
        data = data.groupby(level='pathway').transform(lambda x: x /
                                                       x.sum()).reset_index()
        data.rename(columns={"count": "fraction"}, inplace=True)
        y_order = list(data['TE superfamily'].unique())
        y_order.sort()
        g = sns.catplot(x="fraction", y="TE superfamily",
                        hue="pathway", data=data, height=6, kind="bar",
                        palette="muted", order=y_order)
        g.savefig(output.fig) 

rule plot_dist_HiC:
    input:
        bed="intersection_HiC-PCA_signal-mean_50000bp.bed"
    output:
        fig="fig/plot-distributions_HiC-domain.svg"
    run:
        data = pd.read_csv(input.bed, sep="\t", header=None,
                           names=['chrom', 'start', 'end',
                                  'HiC eigenvalue', 'signal', 'MET / JSD (bit)'],
                           usecols=['HiC eigenvalue', 'signal', 'MET / JSD (bit)'])
        
        signal = data['signal'].str.split('_', expand=True)[[0, 1, 4]]
        signal.rename(columns={0: 'signal', 1: 'context', 4: 'source'}, inplace=True)

        hicdat = pd.concat([data[['HiC eigenvalue', 'MET / JSD (bit)']], signal], axis=1)
        hicdat['context'] = hicdat['context'].replace({'CpG': 'CG'})
        hicdat['signal'] = hicdat['signal'].replace({'mean-met-50000bp': 'MET', 'mean-jsd-50000bp': 'JSD'})
        hicdat['source'] = hicdat['source'].str.replace('.bed', '')
        

        hicdat['HiC domain'] = pd.cut(hicdat['HiC eigenvalue'], bins=[-np.inf, 0, np.inf], labels=['CSD', 'LSD'])
        
        g = sns.catplot(x="MET / JSD (bit)", y="source", hue="HiC domain", col="context", row="signal", kind='box', showfliers=False, data=hicdat)
        g.savefig(output.fig) 

rule plot_density_TE_target_rndobs_count:
    input:
        obs_CMT2="genes-nearby-TEs_{gene}_CMT2-targets.bed",
        obs_RdDM="genes-nearby-TEs_{gene}_RdDM-targets.bed",
        rnd_CMT2=lambda wildcards: "genes-nearby-TEs_random-{size}_draws-1e4_CMT2-targets_count.csv".format(
            size=config["params"]["genes_nearby_targetedTEs"]["randomized"]["size"][wildcards.gene]),
        rnd_RdDM=lambda wildcards: "genes-nearby-TEs_random-{size}_draws-1e4_RdDM-targets_count.csv".format(
            size=config["params"]["genes_nearby_targetedTEs"]["randomized"]["size"][wildcards.gene]),
    output:
        fig="fig/plot-density_genes-nearby-TEs_{gene}_count.svg",
    params:
        size=lambda wildcards: config["params"]["genes_nearby_targetedTEs"]["randomized"]["size"][wildcards.gene]
    run:
        obs = {"CMT2": pd.read_csv(input.obs_CMT2, header=None, sep="\t"),
               "RdDM": pd.read_csv(input.obs_RdDM, header=None, sep="\t")}
        rnd = {"CMT2": pd.read_csv(input.rnd_CMT2, header=0, index_col="sample_id").astype('int32'),
               "RdDM": pd.read_csv(input.rnd_RdDM, header=0, index_col="sample_id").astype('int32')}
        
        sns.set(style="white")
        sns.set_context("paper", font_scale=1.4)
        
        fig, ax = plt.subplots(figsize=(8, 2))
        
        color = dict()
        color["CMT2"] = sns.color_palette("Blues")[-2]
        color["RdDM"] = sns.color_palette("Reds")[-2]

        # Draw the two density plots
        ax = sns.distplot(rnd["CMT2"]["genes"]/params.size,
                          hist=False, kde_kws={"shade": True}, color=color["CMT2"])
        ax = sns.distplot(rnd["RdDM"]["genes"]/params.size,
                          hist=False, kde_kws={"shade": True}, color=color["RdDM"])

        props = dict(boxstyle='round', facecolor='white', alpha=0.7)

        legend_x = 0.75

        for key, data in obs.items():
            obs_ = count_mapped_features(data, col_mapped=-2, sep_mapped=",")
            ax.axvline(obs_[0]/params.size, c=color[key], linewidth=3, linestyle="-")
            legend_y = 0.9 if key == "CMT2" else 0.5
            pvalue = emp_pvalue(rnd[key]["genes"], obs_[0])
            textstr = r"{}: $p={}$".format(key, pvalue)
            ax.text(legend_x, legend_y, textstr,
                    transform=ax.transAxes, size=14, color=color[key],
                    bbox=props, verticalalignment='top',
                    family='monospace')

        ax.set_xlabel(r"fraction of genes nearby TEs ($g$)")
        ax.set_yticks([])
        ax.set_xlim([-0.04, 0.3])

        fig.tight_layout()
        
        ax.figure.savefig(output.fig)

rule plot_density_HiC:
    input:
        obs="HiC-domains_observed_mean.csv",
        rnd="HiC-domains_random_mean.csv",
    output:
        fig="fig/plot-density_HiC-domains_{sig}.svg"
    params:
        sig=lambda wildcards: wildcards.sig.upper(),
        ctx=config["params"]["colormap"]["context"],
    run:
        hicobs = pd.read_csv(input.obs, header=0, usecols=[0,1,2,3,4], index_col=[2,3,4])
        hicrnd = pd.read_csv(input.rnd, header=0, usecols=[0,1,2,3,4], index_col=[2,3,4])

        colors_context = params.ctx
        sig = params.sig

        # sorting avoids performance warning when indexing with .loc
        hicrnd.sort_index(inplace=True) 
        hicobs.sort_index(inplace=True)

        sns.set(style="white")
        sns.set_context("talk")
        
        f, ax = plt.subplots(figsize=(8, 8))
        ax.set_aspect("equal")

        cmap = dict()

        for key, val in colors_context.items():
            cmap[key] = matplotlib.colors.LinearSegmentedColormap.from_list(
                key, [(0, 'white'), (1, val)])

        for ctx in hicrnd.index.unique(level='context'):
            for src in hicrnd.index.unique(level='src'):
                i = (sig, ctx, src)
                ax = sns.kdeplot(hicrnd.loc[i]['mean|e>0'], 
                                 hicrnd.loc[i]['mean|e<0'],
                                 shade=True, shade_lowest=False,
                                 cmap=cmap[ctx])
                ax.scatter(*hicobs.loc[i], c=colors_context[ctx])

        ax.plot(ax.get_xlim(), ax.get_xlim(), 'r--', label='no difference')

        bboxprops = dict(boxstyle='round', facecolor='white', alpha=0.7)

        ax.text(.2, .7, 'higher in CSDs', bbox=bboxprops, 
                ha='center', va='center', transform=ax.transAxes, size=16)

        ax.text(.7, .2, 'higher in LSDs', bbox=bboxprops,
                ha='center', va='center', transform=ax.transAxes, size=16)

        ax.set_xlabel(ax.get_xlabel().replace("mean", "mean {}".format(sig)))
        ax.set_ylabel(ax.get_ylabel().replace("mean", "mean {}".format(sig)))
        ax.legend(loc=2)

        ax.figure.savefig(output.fig)

rule plot_bwsummary_accessibility:
    input:
        "bwsummary_{sig}_accessibility_{bs}kb.npz",
    output:
        "fig/bwsummary_{sig}_accessibility_{bs}kb.svg",
    params:
        options="--corMethod spearman --plotNumbers --colorMap coolwarm",
        label_dat="'DNase leaf' 'NPS leaf' 'ATAC seedling' 'ATAC root'",
        label_sig=lambda wildcards: "'{} CG rosette' '{} CHG rosette' '{} CHH rosette'".format(wildcards.sig.upper(), wildcards.sig.upper(), wildcards.sig.upper()),
    shell:
        "plotCorrelation --corData {input} --plotFile {output} --colorMap coolwarm"
        " --whatToPlot heatmap {params.options} --labels {params.label_dat} {params.label_sig}"

rule plot_bwsummary_histones:
    input:
        "bwsummary_{sig}_histones_{bs}kb.npz",
    output:
        "fig/bwsummary_{sig}_histones_{bs}kb.svg",
    params:
        options="--corMethod spearman --plotNumbers",
        label_dat="'H3K4me1 leaf' 'H3K27me3 leaf' 'H3K27ac leaf' 'H3K9me2 leaf'",
        label_sig=lambda wildcards: "'{} CG rosette' '{} CHG rosette' '{} CHH rosette'".format(wildcards.sig.upper(), wildcards.sig.upper(), wildcards.sig.upper()),
    shell:
        "plotCorrelation --corData {input} --plotFile {output}"
        " --whatToPlot heatmap {params.options} --labels {params.label_dat} {params.label_sig}"

rule plot_profile_chromstate_H3:
    input:
        "matrix-chromstate-histone_{his}.gz"
    output:
        fig="fig/plot-profile-histone_{his}.svg",
        dat="plot-profile-histone_{his}.dat"
    params:
        config["params"]["plot_profile_chromstate"]
    shell:
        "plotProfile --matrixFile {input}"
        " --outFileName {output.fig} --outFileNameData {output.dat}"
        " {params} --samplesLabel {wildcards.his}"

rule plot_profile_TEs_h3k9me2:
    input:
        rules.deeptools_matrix_TEs_h3k9me2.output
    output:
        fig="fig/plot-profile-TEs-histone_H3K9me2.svg",
        dat="plot-profile-TEs-histone_H3K9me2.dat",
    params:
        default=config["params"]["plot_profile_te_superfamilies"],
        regions=TEs_for_h3k9me2,
    shell:
        "plotProfile {params.default} --regionsLabel {params.regions} -m {input} -out {output.fig}"
        " --outFileNameData {output.dat} --samplesLabel H3K9me2"
