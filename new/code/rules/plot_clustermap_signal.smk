rule data_clustermap_signal:
    input:
        "mean-{sig}-{bin}bp_{ctx}_Col-0_wt_{src}.bed"
    output:
        temp("mean-{sig}-{bin}bp_{ctx}_Col-0_wt_{src}.dat")
    params:
        ctx=lambda wildcards: wildcards.ctx.replace("p", ""),
        src=lambda wildcards: wildcards.src.replace("-", " "),
    shell:
        "echo '{params.ctx}' > {output};"
        "echo '{params.src}' >> {output};"
        "cut -f4 {input} >> {output}"

rule paste_clustermap_signal:
    input:
        expand(
            "mean-{{sig}}-{{bin}}bp_{ctx}_Col-0_wt_{src}.dat",
            ctx=config["data"]["cytosine"]["context"],
            src=config["data"]["source"],
        ),
    output:
        "plot-clustermap-mean-{sig}-{bin}bp_Col-0_wt.csv"
    shell:
        "paste {input} > {output}"

rule plot_clustermap_signal:
    input:
        csv="plot-clustermap-mean-{sig}-{bin}bp_Col-0_wt.csv"
    output:
        fig="fig/plot-clustermap-mean-{sig}-{bin}bp_Col-0_wt.svg"
    params:
        cmap=config["params"]["colormap"]["context"],
    run:
        sns.set_context("talk")
        
        data = pd.read_csv(input.csv, header=[0, 1], sep="\t", na_values=["."])

        data.columns.names = ["context", "source"]

        context_values = data.columns.get_level_values("context")
        context_colors = pd.Series(context_values, index=data.columns).map(params.cmap)
        
        g = sns.clustermap(data.corr(method="spearman"),
                           center=0.5, cmap="vlag",
                           row_colors=context_colors, col_colors=context_colors,
                           linewidths=0.75, figsize=(13, 13))

        g.savefig(output.fig)
