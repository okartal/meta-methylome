rule correlation_hic_sig:
    input:
        bed="intersection_HiC-PCA_signal-mean_50000bp.bed"
    output:
        tab="correlation_HiC-PCA_signal-mean_50000bp.csv"
    run:
        names = ["chrom", "start", "end", "HiC value", "signal", "signal value"]

        data = pd.read_csv(input.bed, sep="\t", header=None, names=names)
        
        groups = data.groupby("signal")

        corr = groups.apply(
            lambda g: stats.spearmanr(g[["HiC value", "signal value"]])
        )

        corr_table = pd.DataFrame(
            corr.values.tolist(), index=corr.index
        ).reset_index()

        result = corr_table.signal.str.split("_", 5, expand=True)

        result.columns = ["signal", "context", "etype", "gtype", "source"]

        result["spearmanr"] = corr_table["correlation"]
        result["-log10(pvalue)"] = -np.log10(corr_table["pvalue"])
        
        result.to_csv(output.tab, index=False, header=True)

