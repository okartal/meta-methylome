rule top_msc_genes_fraction:
    input:
        all="coverage-{size}bp_gene_protein-coding-0kb-flank.bed",
        msc="coverage-{size}bp_gene_protein-coding-0kb-flank_{pct}-msc_{ctx}_{src}_{etype}_{gtype}.bed",
    output:
        "fraction-{size}bp_gene_protein-coding-0kb-flank_{pct}-msc_{ctx}_{src}_{etype}_{gtype}.bedg"
    shell:
        "paste {input.all} {input.msc}"
        " | cut -f1-4,11"
        " | awk 'BEGIN{{OFS=\"\\t\"}} {{if($4==0) f=0; else f=$5/$4}} {{print $1, $2, $3, f}}'"
        " > {output}"
