rule deeptools_matrix_te_superfamilies:
    input:
        cpg="{signal}_CpG_Col-0_wt_{source}.bw",
        chg="{signal}_CHG_Col-0_wt_{source}.bw",
        chh="{signal}_CHH_Col-0_wt_{source}.bw",
        regions=["feature_TE_" + name + ".bed" for name in config["data"]["te_superfamilies"]]
    output:
        "feature_TE_{signal}-profile_Col-0_wt_{source}.mat.gz"
    params:
        config["params"]["deeptools_matrix_te_superfamilies"]
    threads:
        config["threads"]["deeptools_matrix_te_superfamilies"]
    shell:
        "computeMatrix scale-regions -S {input.cpg} {input.chg} {input.chh}"
        " -R {input.regions} {params} -p {threads} -o {output}"

rule deeptools_matrix_genes:
    input:
        s=expand("{{sig}}_{{ctx}}_Col-0_wt_{src}.bw", src=config["data"]["source"]),
        r=["feature_gene_protein-coding-0kb-flank_not-overlapping-TE-genes.bed",
           "feature_gene_protein-coding-0kb-flank_overlapping-TE-genes.bed",
           "feature_gene_transposable-element.bed"],
    output:
        "genes_{sig}_{ctx}.mat.gz"
    params:
        config["params"]["deeptools_matrix_genes"]
    threads:
        config["threads"]["deeptools_matrix_genes"]
    shell:
        "computeMatrix scale-regions {params} -p {threads} -S {input.s} -R {input.r} -o {output}"

rule deeptools_matrix_te_targets:
    input:
        s=expand("{{sig}}_{ctx}_Col-0_wt_{{src}}.bw", ctx=config["data"]["cytosine"]["context"]),
        r=["feature_TE_CMT2_targets.bed", "feature_TE_RdDM_targets.bed"],
    output:
        "te_targets_{sig}_{src}.mat.gz"
    params:
        config["params"]["deeptools_matrix_te_targets"]
    threads:
        config["threads"]["deeptools_matrix_te_targets"]
    shell:
        "computeMatrix scale-regions {params} -p {threads} -S {input.s} -R {input.r} -o {output}"

rule deeptools_matrix_expression:
    input:
        signal=[
            "{{sig}}_{ctx}_{{etype}}_{{gtype}}_{{source}}.bw".format(ctx=c) for c in config["data"]["cytosine"]["context"]
            ],
        region=[
            "feature_gene_expression-{tissue}-top50.bed",
            "feature_gene_expression-{tissue}-bottom50.bed",
        ],
    output:
        "profile_gene_expression-{tissue}_{sig}_{etype}_{gtype}_{source}.mat.gz"
    params:
        config["params"]["deeptools_matrix_expression"]
    threads:
        config["threads"]["deeptools_matrix_expression"]
    shell:
        "computeMatrix scale-regions -S {input.signal}"
        " -R {input.region} {params} -p {threads} -o {output}"

# rule bw_mean:
#     input:
#         b1="../primary/{sample}_rep1.bw",
#         b2="../primary/{sample}_rep2.bw",
#     output:
#         "{sample}.bw"
#     params:
#         "--operation mean --outFileFormat bigwig"
#     shell:
#         "bigwigCompare --bigwig1 {input.b1} --bigwig2 {input.b2}"
#         " --outFileName {output} {params}"
        
# rule bw_log2ratio:
#     input:
#         b1="{sample}_{signal}.bw",
#         b2="{sample}_{background}.bw",
#     output:
#         "{sample}_{signal}-{background}_log2ratio.bw"
#     params:
#         "--operation log2 --outFileFormat bigwig"
#     shell:
#         "bigwigCompare --bigwig1 {input.b1} --bigwig2 {input.b2}"
#         " --outFileName {output} {params}"
    
rule bwsummary_accessibility:
    input:
        dnase="../primary/Ath_leaf_DNase.bw",
        nps="../primary/Ath_leaf_NPS.bw",
        atac_seed="Col-0_50k_seedling_nuclei_FANS-ATAC-seq.bw",
        atac_root="Col-0_50k_root_nuclei_FANS-ATAC-seq.bw",
        sig=["{{sig}}_{ctx}_Col-0_wt_rosette.bw".format(ctx=c) for c in config["data"]["cytosine"]["context"]],
    output:
        "bwsummary_{sig}_accessibility_{bs}kb.npz"
    params:
        binsize=lambda wildcards: int(wildcards.bs) * 1000
    shell:
        "multiBigwigSummary bins -b"
        " {input.dnase} {input.nps} {input.atac_seed} {input.atac_root}"
        " {input.sig} -o {output}"

rule bwsummary_histones:
    input:
        H3K4me1="../primary/Ath_leaf_H3K4me1.bw",
        H3K27me3="../primary/Ath_leaf_H3K27me3.bw",
        H3K27ac="../primary/Ath_leaf_H3K27ac.bw",
        H3K9me2_wtype="Col-0_H3K9me2-H3_log2ratio.bw",
        sig=["{{sig}}_{ctx}_Col-0_wt_rosette.bw".format(ctx=c) for c in config["data"]["cytosine"]["context"]],
    output:
        "bwsummary_{sig}_histones_{bs}kb.npz",
    params:
        binsize=lambda wildcards: int(wildcards.bs) * 1000
    shell:
        "multiBigwigSummary bins -b"
        " {input.H3K4me1} {input.H3K27me3} {input.H3K27ac} {input.H3K9me2_wtype}"
        " {input.sig} -o {output}"

rule computeMatrix_chromstate_H3K27ac:
    input:
        sig="../primary/Ath_buds_H3K27ac.bw",
        reg=["../primary/chromatin-state{num}.bed".format(num=n) for n in range(1,10)],
    output:
        "matrix-chromstate-histone_H3K27ac.gz"
    params:
        "-b 2000 -a 2000 -m 4000 --binSize 100"
    shell:
        "computeMatrix scale-regions {params}"
        " -S {input.sig} -R {input.reg} -o {output}"

rule computeMatrix_chromstate_H3K9me2:
    input:
        sig="Col-0_H3K9me2-H3_log2ratio.bw",
        reg=["../primary/chromatin-state{num}.bed".format(num=n) for n in range(1,10)],
    output:
        "matrix-chromstate-histone_H3K9me2.gz"
    params:
        "-b 2000 -a 2000 -m 4000 --binSize 100"
    shell:
        "computeMatrix scale-regions {params}"
        " -S {input.sig} -R {input.reg} -o {output}"

TEs_for_h3k9me2 = config["data"]["te_superfamilies"] + ["CMT2_targets", "RdDM_targets"]

rule deeptools_matrix_TEs_h3k9me2:
    input:
        sig="Col-0_H3K9me2-H3_log2ratio.bw",
        reg=["feature_TE_" + name + ".bed" for name in TEs_for_h3k9me2]
    output:
        "matrix-TEs-histone_H3K9me2.gz"
    params:
        config["params"]["deeptools_matrix_te_superfamilies"]
    shell:
        "computeMatrix scale-regions -S {input.sig}"
        " -R {input.reg} {params} -o {output}"
