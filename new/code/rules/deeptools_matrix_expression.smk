rule deeptools_matrix_expression:
    input:
        signal=[
            "{{sig}}_{ctx}_{{etype}}_{{gtype}}_{{source}}.bw".format(ctx=c) for c in config["data"]["cytosine"]["context"]
            ],
        region=[
            "feature_gene_expression-{tissue}-top50.bed",
            "feature_gene_expression-{tissue}-bottom50.bed",
        ],
    output:
        "profile_gene_expression-{tissue}_{sig}_{etype}_{gtype}_{source}.mat.gz"
    params:
        config["params"]["deeptools_matrix_expression"]
    threads:
        config["threads"]["deeptools_matrix_expression"]
    shell:
        "computeMatrix scale-regions -S {input.signal}"
        " -R {input.region} {params} -p {threads} -o {output}"
