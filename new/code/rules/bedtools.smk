rule genomebins:
    input:
        "../primary/TAIR10_chromSize.txt"
    output:
        "genomebin_{binsize}bp.bed"
    shell:
        "bedtools makewindows -g {input}"
        " -w {wildcards.binsize}"
        " > {output}"
        
rule get_lmc:
    input:
        hmc="HMC-lb20ub80_{ctx}_{etype}_{gtype}_{src}.bed",
        mmc="MMC-lb20ub80_{ctx}_{etype}_{gtype}_{src}.bed",
        msc="MSC-lb20ub80_{ctx}_{etype}_{gtype}_{src}.bed",
        div="div_{ctx}_{etype}_{gtype}_{src}.bed.gz"
    output:
        lmc=temp("LMC-lb20ub80_{ctx}_{etype}_{gtype}_{src}.bed")
    run:
        hmc = BedTool(input.hmc)
        mmc = BedTool(input.mmc)
        msc = BedTool(input.msc)
        div = BedTool(input.div)

        (div
         .subtract(hmc, A=True)
         .subtract(mmc, A=True)
         .subtract(msc, A=True)
         .each(bedtools.add_met)
         .saveas(output.lmc))

rule count_cytotype_features:
    input:
        hmc="HMC-lb20ub80_{ctx}_{etype}_{gtype}_{src}.bed",
        mmc="MMC-lb20ub80_{ctx}_{etype}_{gtype}_{src}.bed",
        msc="MSC-lb20ub80_{ctx}_{etype}_{gtype}_{src}.bed",
        lmc="LMC-lb20ub80_{ctx}_{etype}_{gtype}_{src}.bed",
        feature="feature_{feature}.bed"
    output:
        cnt=temp("count-ctype_{feature}_{ctx}_{etype}_{gtype}_{src}.bed")
    params:
        min=config["params"]["count_cytotype_features"]["min coverage"]
    run:
        hmc = BedTool(input.hmc)
        mmc = BedTool(input.mmc)
        msc = BedTool(input.msc)
        lmc = BedTool(input.lmc)

        feature = BedTool(input.feature)    
        
        (feature
         .coverage(hmc, counts=True, sorted=True)
         .coverage(mmc, counts=True, sorted=True)
         .coverage(msc, counts=True, sorted=True)
         .coverage(lmc, counts=True, sorted=True)
         .filter(bedtools.coverage_filter, dim=4, min_fraction=params.min)
         .saveas(output.cnt))

rule prob_cytotype_features:
    """Get estimate for probability of cytotypes across features.

    Uses additive smoothing.
    """
    input:
        cnt="count-ctype_{feature}_{ctx}_{etype}_{gtype}_{src}.bed"
    output:
        prb="probability-ctype_{feature}_{ctx}_{etype}_{gtype}_{src}.bed"
    params:
        alpha=config["params"]["prob_cytotype_features"]["pseudocount"]
    run:
        counts = BedTool(input.cnt)
        
        (counts
         .each(bedtools.smooth, dim=4, alpha=params.alpha)
         .saveas(output.prb))

rule coverage_feature:
    input:
        windows="genomebin_{size}bp.bed",
        feature="feature_{feature}.bed"
    output:
        density="coverage-{size}bp_{feature}.bed"
    run:
        windows = BedTool(input.windows)
        feature = BedTool(input.feature)    
        
        (windows
         .coverage(feature, sorted=True)
         .saveas(output.density))

rule genes_byTE:
    input:
        genes_all="feature_gene_protein-coding-0kb-flank.bed",
        TE_genes="feature_gene_transposable-element.bed",
        genome="../primary/TAIR10_chromSize.txt",
    output:
        genes_TE="feature_gene_protein-coding-0kb-flank_overlapping-TE-genes.bed",
        genes_notTE="feature_gene_protein-coding-0kb-flank_not-overlapping-TE-genes.bed",
    run:
        a = BedTool(input.genes_all)
        b = BedTool(input.TE_genes)

        genes_TE = a.intersect(b, sorted=True, stream=True)
        genes_notTE = a.subtract(b, A=True, sorted=True, g=input.genome, stream=True)

        genes_TE.saveas(output.genes_TE)
        genes_notTE.saveas(output.genes_notTE)

rule coverage_chromstate:
    input:
        windows="genomebin_{size}bp.bed",
        feature="../primary/chromatin-state{num}.bed.gz",
    output:
        density="coverage-{size}bp_chromstate{num}.bed"
    run:
        windows = BedTool(input.windows)
        feature = BedTool(input.feature)    
        
        (windows
         .coverage(feature, sorted=True)
         .saveas(output.density))

rule fraction_chromstate:
    input:
        "coverage-{size}bp_chromstate{num}.bed"
    output:
        "fraction-{size}bp_chromstate{num}.bedg"
    shell:
        "cut -f 1-3,7 {input} > {output}"

rule reldist_genes_targetedTEs:
    input:
        a="feature_gene_{subtype}.bed",
        b="feature_TE_{path}_targets.bed",
    output:
        temp("reldist_gene_{subtype}_TE_{path}.csv")
    shell:
        "bedtools reldist -a {input.a} -b {input.b}"
        " | awk 'NR<2{{print $0\"\\tgene\\tTE\"; next}}"
        " {{print $0\"\\t{wildcards.subtype}\\t{wildcards.path}-targeted\"}}'"
        " > {output}"

rule concat_reldist:
    input:
        expand(
            "reldist_gene_{subtype}_TE_{path}.csv",
            subtype=config["params"]["reldist_genes_targetedTEs"].keys(),
            path=config["params"]["genes_nearby_targetedTEs"]["targets"],
        ),
    output:
        "reldist_gene_TE.csv"
    shell:
        "awk 'FNR==1 && NR!=1{{next;}}{{print}}' {input} > {output}"
