rule top_msc_genes:
    """Annotate genes by whether they belong to the top quantile of high
    MSC proportion.

    """
    input:
        bed=[
            "probability-ctype_gene_protein-coding-{{slop}}kb-flank_{ctx}_{{etype}}_{{gtype}}_{src}.bed".format(
                ctx=c, src=s
            )
            for c in config["data"]["cytosine"]["context"]
            for s in config["data"]["source"]
            ]
    output:
        bed="top-msc_gene_protein-coding-{slop}kb-flank_{etype}_{gtype}.bed"
    params:
        quantiles=config["params"]["top_msc_genes"]["quantiles"],
        cols=config["data"]["top_msc_genes"]["names"]
    run:

        dtype = {k: v for k, v in zip(
            config["data"]["bed6_prob_ctype"]["names"],
            config["data"]["bed6_prob_ctype"]["dtype"])
        }
        
        data = [
            pd.read_csv(
                i,
                sep="\t",
                header=None,
                names=config["data"]["bed6_prob_ctype"]["names"],
                dtype=dtype
            )
            for i in input.bed
        ]

        for q in params.quantiles:
            pct = round(100*(1 - q))
            col = "top{}pct".format(pct)
            for d in data:
                d[col] = (d["MSC"] > d["MSC"].quantile(q)).astype(int)

        keys = [
            (c.replace("p", ""), s.replace("-", " "))
            for c in config["data"]["cytosine"]["context"]
            for s in config["data"]["source"]            
        ]
        
        data_all = pd.concat(
            data,
            keys=keys,
            names=["context", "source"]
        ).reset_index()[params.cols]

        data_all.to_csv(output.bed, sep="\t", index=False, header=True)

rule top_msc_genes_unique:
    """Get list of unique genes for easier submission for GO categories.
    """
    input:
        "genes-unique-source_MSC-{top}_{ctx}_{etype}_{gtype}.csv"
    output:
        "genes-unique_{src}_MSC-{top}_{ctx}_{etype}_{gtype}.txt"
    params:
        src=lambda wildcards: wildcards.src.replace("-", " ")
    shell:
        "grep \"{params.src}\" {input} | cut -d, -f1 > {output}"
