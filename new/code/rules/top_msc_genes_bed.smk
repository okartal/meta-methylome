rule top_msc_genes_bed:
    input:
        "top-msc_gene_protein-coding-{slop}kb-flank_{etype}_{gtype}.bed"
    output:
        "feature_gene_protein-coding-{slop}kb-flank_{pct}-msc_{ctx}_{src}_{etype}_{gtype}.bed"
    params:
        col_ctx=config["data"]["top_msc_genes"]["names"].index("context") + 1,
        col_src=config["data"]["top_msc_genes"]["names"].index("source") + 1,
        col_pct=lambda wildcards: config["data"]["top_msc_genes"]["names"].index(wildcards.pct) + 1,
        src=lambda wildcards: wildcards.src.replace("-", " "),
    shell:
        "awk -F \"\\t\" '"
        " ${params.col_ctx}==\"{wildcards.ctx}\" &&"
        " ${params.col_src}==\"{params.src}\" &&"
        " ${params.col_pct}==1"
        " ' {input} | cut -f1-4 > {output}"
