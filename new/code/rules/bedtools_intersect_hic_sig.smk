rule bedtools_intersect_hic_sig:
    input:
        a="HiC-PCA.bedg",
        b=expand(
            "mean-{sig}-50000bp_{ctx}_{etype}_{gtype}_{src}.bed",
            sig=config["data"]["diversity"],
            ctx=config["data"]["cytosine"]["context"],
            etype=config["data"]["genome"]["ecotype"],
            gtype=config["data"]["genome"]["genotype"],
            src=config["data"]["source"],            
        )
    output:
        "intersection_HiC-PCA_signal-mean_50000bp.bed"
    params:
        config["params"]["bedtools_intersect_hic_sig"]
    shell:
        "bedtools intersect {params} -a {input.a} -b {input.b}"
        " | cut -f1-5,9"
        " | sed 's/_50000bp.bed//'"
        " > {output}"
