rule bedtools_map_mean_genomebin:
    input:
        a="genomebin_{size}bp.bed",
        b="{sig}_{ctx}_{etype}_{gtype}_{src}.bedg"
    output:
        bed="mean-{sig}-{size}bp_{ctx}_{etype}_{gtype}_{src}.bed"
    params:
        config["params"]["bedtools_map_jsd_genomebin"]
    shell:
        "bedtools map {params} -a {input.a} -b {input.b}"
        " > {output.bed}"
