rule genes_nearby_targetedTEs:
    input:
        gene="feature_gene_{gene}.bed",
        feat="feature_TE_{pathway}_targets.bed",
        genome="../primary/TAIR10_chromSize.txt",
    output:
        bed="genes-nearby-TEs_{gene}_{pathway}-targets.bed"
    params:
        flank=config["params"]["genes_nearby_targetedTEs"]["flank"]
    run:
        gene = BedTool(input.gene)
        feat = BedTool(input.feat)
        nearby = bedtools.nearby(gene, feat, genome=input.genome, flank=params.flank)
        nearby.saveas(output.bed)

rule genes_random_nearby_targetedTE:
    input:
        gene="feature_gene_protein-coding-0kb-flank_non-transposable-element.bed",
        feat="feature_TE_{pathway}_targets.bed",
        genome="../primary/TAIR10_chromSize.txt",
    output:
        count="genes-nearby-TEs_random-{size}_draws-{draws}_{pathway}-targets_count.csv",
        dist="genes-nearby-TEs_random-{size}_draws-{draws}_{pathway}-targets_TEdist.csv",
    params:
        size=lambda wildcards: int(wildcards.size),
        draws=lambda wildcards: int(float(wildcards.draws)),
        flank=config["params"]["genes_nearby_targetedTEs"]["flank"],
    run:
        gene_all = pd.read_csv(input.gene, sep="\t", header=None)
        feat = BedTool(input.feat)

        samples = bedtools.random_samples(gene_all, size=params.size, n=params.draws)

        with open(output.count, "a") as out_count, open(output.dist, "a") as out_dist:
            out_count.write("genes,TEs,sample_id\n")
            out_dist.write("count,frequency,sample_id\n")
            for i, gene in enumerate(samples, 1):
                tmpfile = "sample-{i}_{s}_{d}_{p}.tmp".format(i=i,
                                                              s=wildcards.size,
                                                              d=wildcards.draws,
                                                              p=wildcards.pathway)
                bedtools.nearby(BedTool.from_dataframe(gene).sort(stream=True),
                                feat,
                                genome=input.genome,
                                flank=params.flank).saveas(tmpfile)
                try:
                    nearby_gene = pd.read_csv(tmpfile, sep="\t", header=None)
                    gene_size, te_size = count_mapped_features(nearby_gene, col_mapped=-2, sep_mapped=",")
                    dist = dist_mapped_features(nearby_gene, col_count=-1, targetset_size=len(gene))
                    dist["sample_id"] = i
                    out_count.write("{},{},{}\n".format(gene_size, te_size, i))
                    dist.to_csv(out_dist, header=False, index=False)
                except pd.errors.EmptyDataError:
                    out_count.write("{},{},{}\n".format(0, 0, i))
                    out_dist.write("{},{},{}\n".format(0, len(gene), i))
                finally:
                    os.remove(tmpfile)

rule genes_nearby_chromstate:
    """Get genes nearby to chromosome states.
    """
    input:
        gene="feature_gene_protein-coding-0kb-flank_non-transposable-element.bed",
        feat="../primary/chromatin-state{num}.bed.gz",
        genome="../primary/TAIR10_chromSize.txt",
    output:
        bed="feature_gene_nearby-chromstate{num}.bed"
    params:
        flank = config["params"]["genes_nearby_chromstate"]["flanksize"],
        length= config["params"]["genes_nearby_chromstate"]["minlength"],
    run:
        gene = BedTool(input.gene)
        feat = BedTool(input.feat).filter(lambda f: len(f) >= params.length)
        nearby = bedtools.nearby(gene, feat, genome=input.genome, flank=params.flank)
        nearby.saveas(output.bed)
        
   
