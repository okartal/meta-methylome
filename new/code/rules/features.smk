rule feature_te:
    input:
        "transposable_element.gff"
    output:
        temp("feature_TE.bed")
    shell:
        "cut -f1,4,5,9 {input} > {output}"

rule feature_te_superfamilies:
    input:
        "../primary/TAIR10_Transposable_Elements.txt.gz"
    output:
        temp(["feature_TE_" + name + ".bed" for name in config["data"]["te_superfamilies"]])
    shell:
        "python ../../code/scripts/get_te_superfamilies.py {input}"

rule feature_gene_proteincoding:
    input:
        gff="../primary/Araport11_GFF3_genes_transposons.201606.v2.sorted.gff.gz",
        genome="../primary/TAIR10_chromSize.txt"
    output:
        bed=temp("feature_gene_protein-coding-{slop}kb-flank.bed")
    run:
        gff = BedTool(input.gff)

        assert gff.file_type == "gff"

        b = int(wildcards.slop) * 1000
        
        (gff
         .filter(lambda f: f[0] in set("1 2 3 4 5"))
         .filter(lambda f: f[2] == "gene")
         .filter(lambda f: f.attrs["locus_type"]=="protein_coding")
         .slop(b=b, g=input.genome)
         .each(gff2bed, name_field="ID")
         .saveas(output.bed))

rule feature_gene_transposon:
    input:
        gff="../primary/Araport11_GFF3_genes_transposons.201606.v2.sorted.gff.gz",
        genome="../primary/TAIR10_chromSize.txt"
    output:
        bed=temp("feature_gene_transposable-element.bed")
    run:
        gff = BedTool(input.gff)

        assert gff.file_type == "gff"

        (gff
         .filter(lambda f: f[0] in set("1 2 3 4 5"))
         .filter(lambda f: f[2] == "transposable_element_gene")
         .each(gff2bed, name_field="ID")
         .saveas(output.bed))

rule feature_promoters:
    input:
        gff="promoter_{category}.gff",
        genome="../primary/TAIR10_chromSize.txt"
    output:
        bed=temp("feature_promoter_{category}_{type}.bed")
    params:
        b=config["params"]["feature_promoters"]["slop"]
    run:
        gff = BedTool(input.gff)

        assert gff.file_type == "gff"
        
        (gff
         .filter(lambda f: f[0] in set("1 2 3 4 5"))
         .filter(lambda f: f.attrs["Name"]==wildcards.type)
         .slop(b=int(params.b), g=input.genome)
         .each(gff2bed, name_field="ID")
         .saveas(output.bed))

# to see promoter types:
# cut -f9 promoter_all.gff | cut -f2 -d";" | cut -f2 -d= | sort | uniq -c | awk '{OFS="\t"} {print $1,$2}' | sort -k1,1nr

rule TE_target_bed:
    """Make BED for TEs targeted by CMT2 and RdDM pathways of DNA methylation.
    : primary data from Kawakatsu et al. 2016, provided by Eriko Sasaki (Nordborg Lab)
    """
    input:
        gff="transposable_element.gff",
        targets="../primary/{pathway}_target_TEs.txt",
    output:
        gff=temp("feature_TE_{pathway}_targets.gff"),
        bed=temp("feature_TE_{pathway}_targets.bed"),
    run:
        gff = BedTool(input.gff)
        assert gff.file_type == "gff"

        with open(input.targets, "r") as target_list:
            targets = {name.strip() for name in target_list if name.strip()}
            (gff
             .filter(lambda f: f.attrs["ID"] in targets)
             .saveas(output.gff)
             .each(gff2bed, name_field="ID")
             .saveas(output.bed))

rule feature_div:
    input:
        div="div_{ctx}_{etype}_{gtype}_{src}.bed.gz",
        bed="feature_{feature}.bed",
    output:
        temp("div-feature_{feature}_{ctx}_{etype}_{gtype}_{src}.bed")
    shell:
        "set +o pipefail;"
        " zcat {input.div} | head -n1 > {output};"
        " bedtools intersect -a {input.div} -b {input.bed}"
        " >> {output}"
        
rule TE_target_superfamilies:
    input:
        "feature_TE_{path}_targets.gff"
    output:
        temp("TE-superfamilies_in_{path}-targets.csv")
    shell:
        "grep -Po 'Alias=\K[^;]*' {input} | sort | uniq -c"
        " | awk '{{OFS=\",\"}} {{print $1,$2,\"{wildcards.path}\"}}'"
        " > {output}"

rule concat_TE_target_superfamilies:
    input:
        expand(
            "TE-superfamilies_in_{path}-targets.csv",
            path=config["params"]["genes_nearby_targetedTEs"]["targets"]
        )
    output:
        temp("TE-superfamilies_for_targets.csv")
    shell:
        "echo 'count,TE superfamily,pathway' > {output};"
        "cat {input} >> {output}"

rule feature_gene_expression:
    input:
        exp="../primary/ExpressionAnglerOutput_{tissue}_{expression}.txt",
        bed="feature_gene_protein-coding-0kb-flank.bed"
    output:
        bed=temp("feature_gene_expression-{tissue}-{expression}.bed")
    run:
        all_genes = BedTool(input.bed)

        score = {gene: score for gene, score in
                 io.get_expressed_genes(input.exp)}

        (all_genes
         .filter(lambda f: f[3] in score.keys())
         .each(bedtools.update_scores, score)
         .saveas(output.bed))
