rule bw_to_bedg_sig:
    input:
        "{sig}_{ctx}_{etype}_{gtype}_{src}.bw"
    output:
        "{sig}_{ctx}_{etype}_{gtype}_{src}.bedg"
    shell:
        "bigWigToBedGraph {input} {output}"
