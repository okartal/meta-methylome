rule transposon_fraction:
    input:
        all="coverage-{size}bp_TE.bed",
        fam="coverage-{size}bp_TE_{family}.bed",
    output:
        "fraction-{size}bp_TE_{family}.bedg"
    shell:
        "paste {input.all} {input.fam}"
        " | cut -f1-4,11"
        " | awk 'BEGIN{{OFS=\"\\t\"}} {{if($4==0) f=0; else f=$5/$4}} {{print $1, $2, $3, f}}'"
        " > {output}"
