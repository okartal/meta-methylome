rule observed_hic_domains:
    input:
        hic="HiC-PCA.bedg",
        sig="{sig}_{ctx}_{etype}_{gtype}_{src}.bedg",
    output:
        "observed_HiC-domains_{sig}_{ctx}_{etype}_{gtype}_{src}.bed"
    shell:
        "bedtools map -c 4 -o sum,count -a {input.hic} -b {input.sig} > {output}"

rule randomized_hic_domains:
    input:
        "observed_HiC-domains_{sig}_{ctx}_{etype}_{gtype}_{src}.bed"
    output:
        temp("random-{i}_HiC-domains_{sig}_{ctx}_{etype}_{gtype}_{src}.bed")
    shell:
        "set +o pipefail;"
        " paste <(cut -f1-3 {input}) <(cut -f4 {input} | shuf) <(cut -f5-6 {input})"
        " > {output}"
    
rule mean_hic_domains:
    input:
        "{conf}_HiC-domains_{sig}_{ctx}_{etype}_{gtype}_{src}.bed"
    output:
        temp("{conf}-mean_HiC-domains_{sig}_{ctx}_{etype}_{gtype}_{src}.csv")
    params:
        ctx=lambda wildcards: wildcards.ctx.replace("CpG", "CG"),
        sig=lambda wildcards: wildcards.sig.upper(),
        src=lambda wildcards: wildcards.src.replace("-", " "),
    shell:
        "awk '{{ OFS=\",\" }} {{"
        " if ($4 > 0)"
        " {{ sumPos += $5; countPos += $6 }}"
        " else"
        " {{ sumNeg += $5; countNeg += $6 }} }} END"
        " {{ print sumPos/countPos,sumNeg/countNeg,\"{params.sig}\",\"{params.ctx}\",\"{params.src}\",\"{wildcards.etype}\",\"{wildcards.gtype}\" }}'"
        " {input} > {output}"

rule concat_mean_random:
    """
    Note: We had to use printf here because expanding many arguments
    leads to an "Argument list too long" error. See:
    - https://unix.stackexchange.com/questions/38955/argument-list-too-long-for-ls
    - https://www.in-ulm.de/~mascheck/various/argmax/
    """
    input:
        expand(
            "random-{i}-mean_HiC-domains_{sig}_{ctx}_{etype}_{gtype}_{src}.csv",
            i=range(1000),
            sig=config["data"]["diversity"],
            ctx=config["data"]["cytosine"]["context"],
            etype=config["data"]["genome"]["ecotype"],
            gtype=config["data"]["genome"]["genotype"],
            src=config["data"]["source"],
        )
    output:
        "HiC-domains_random_mean.csv"
    shell:
        "echo 'mean|e>0,mean|e<0,signal,context,src,etype,gtype' > {output};"
        "printf '%s\\0' random-*-mean_HiC-domains*.csv | xargs -0 cat >> {output}"

rule concat_mean_observed:
    input:
        expand(
            "observed-mean_HiC-domains_{sig}_{ctx}_{etype}_{gtype}_{src}.csv",
            sig=config["data"]["diversity"],
            ctx=config["data"]["cytosine"]["context"],
            etype=config["data"]["genome"]["ecotype"],
            gtype=config["data"]["genome"]["genotype"],
            src=config["data"]["source"],
        ),
    output:
        "HiC-domains_observed_mean.csv"
    shell:
        "echo 'mean|e>0,mean|e<0,signal,context,src,etype,gtype' > {output};"
        "cat {input} >> {output}"

rule pvalue_mean_domains:
    """Empirical p-value for mean difference between CSDs and LSDs"""
    input:
        obs="HiC-domains_observed_mean.csv",
        rnd="HiC-domains_random_mean.csv",
    output:
        csv="tab-pvalues_HiC-domains_mean.csv",
        tex="tab-pvalues_HiC-domains_mean.tex",
    run:
        hicobs = pd.read_csv(input.obs, header=0, usecols=[0,1,2,3,4], index_col=[2,3,4])
        hicrnd = pd.read_csv(input.rnd, header=0, usecols=[0,1,2,3,4], index_col=[2,3,4])

        hicrnd.sort_index(inplace=True) 
        hicobs.sort_index(inplace=True)

        hicrnd_diff = hicrnd["mean|e<0"] - hicrnd["mean|e>0"]
        hicobs_diff = hicobs["mean|e<0"] - hicobs["mean|e>0"]
        
        g = hicrnd_diff.groupby(level=['signal', 'context', 'src'])

        pvalues = g.apply(lambda x: emp_pvalue(x, hicobs_diff.loc[x.name]))

        pvalues.name = 'p-value'

        pvalues.to_csv(output.csv, header=True, index=True)
        pvalues.to_latex(output.tex)

        
