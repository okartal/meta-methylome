rule count_ctype:
    input:
        expand(
            "{cty}-lb20ub80_{ctx}_Col-0_wt_{src}.bed",
            cty=["HMC", "MMC", "LMC", "MSC"],
            ctx=config["data"]["cytosine"]["context"],
            src=config["data"]["source"]
        )
    output:
        "count-ctype_genome.csv"
    shell:
        "echo 'count,C-type,context,ecotype,genotype,source'"
        " > {output}"
        " ; wc -l {input}"
        " | awk 'BEGIN{{OFS=\",\"}} {{print $1, $2}}'"
        " | tr '_' ','"
        " | sed 's/-lb20ub80//'"
        " | sed 's/.bed//'"
        " | head -n -1"
        " >> {output}"

rule proportion_ctype:
    input:
        csv="count-ctype_genome.csv"
    output:
        csv="proportion-ctype_genome.csv",
        tex="proportion-ctype_genome.tex",
    run:
        data = pd.read_csv(input.csv, header=0, usecols=[0,1,2,5])

        data["count"] = data.groupby(["source", "context"])["count"].transform(lambda x: 100 * x/x.sum())

        data.rename(columns={"count": "proportion (%)"}, inplace=True)

        data["context"] = data["context"].replace({"CpG": "CG"})

        data_out = (data
                    .sort_values(["source", "context", "C-type"])
                    .set_index(["source", "context", "C-type"]))

        data_out.to_csv(output.csv)
        data_out.unstack("context").to_latex(output.tex)
