rule cat_hic:
    input:
        expand(
            "HiC-PCA_{chromosome_arm}.bedg",
            chromosome_arm=config["params"]["hicdata2bedg"]
        )
    output:
        "HiC-PCA.bedg"
    shell:
        "cat {input} | sort -k1,1 -k2,2n -k3,3n > {output}"
