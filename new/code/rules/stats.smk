rule stats:
    input:
        ["div_{ctx}_{{etype}}_{{gtype}}_{{src}}.bed.gz".format(ctx=c)
         for c in config["data"]["cytosine"]["context"]]
    output:
        stats="stats_{etype}_{gtype}_{src}.csv",
        tests="tests-mannwhitneyu_jsd-between-context_{etype}_{gtype}_{src}.csv"
    run:
        keys = config["data"]["cytosine"]["context"]

        dtype = {k: v for k, v in zip(
            config["data"]["shannon"]["names"],
            config["data"]["shannon"]["dtype"])
        }
        
        data = pd.concat(
            (pd.read_csv(
                i,
                sep="\t",
                usecols=["#chrom", "JSD_bit_", "5mC", "C"],
                header=0,
                dtype=dtype
            ).rename(columns={'JSD_bit_': 'JSD (bit)'}) for i in input),
            keys=keys,
            names=["context"]
        ).reset_index().replace({"context": "CpG"}, value="CG")

        data["MET"] = data["5mC"]/(data["5mC"] + data["C"])

        data_grouped = data.groupby(["#chrom", "context"])[["JSD (bit)", "MET"]].describe()

        data_stats = pd.melt(
            data_grouped.reset_index(),
            id_vars=["#chrom", "context"],
            var_name=["score", "statistic"]
        )

        data_stats.to_csv(output.stats, index=False, header=True)

        pd.DataFrame(
            test_mannwhitneyu(data.groupby("context"), value="JSD (bit)")
        ).to_csv(output.tests, index=False)
        
rule stats_latex:
    input:
        csv=["stats_{{etype}}_{{gtype}}_{src}.csv".format(src=s) for s in config["data"]["source"]]
    output:
        tex="stats_{etype}_{gtype}.tex"
    params:
        keys=config["data"]["source"]
    run:
        
        data = (pd.read_csv(i, header=0) for i in input.csv)

        data_concat = pd.concat(data, keys=params.keys, names=["source"])

        data_concat.to_latex(output.tex)
