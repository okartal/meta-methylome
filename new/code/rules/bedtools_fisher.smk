rule bedtools_fisher:
    input:
        top="top-msc_gene_protein-coding_Col-0_wt.bed",
        genome="../primary/TAIR10_chromSize.txt",
        feature="feature_{feat}.bed"
    output:
        txt="tests/fisher_{top}-metastable-genes_{feat}_{ctx}_{etype}_{gtype}_{src}.txt",
        dat=temp("fisher_{top}-metastable-genes_{feat}_{ctx}_{etype}_{gtype}_{src}.txt")
    run:
        df = pd.read_csv(input.top, header=0, sep="\t")

        top = "context=='{}' and source=='{}' and {}==1".format(
            wildcards.ctx, wildcards.src, wildcards.top
        )

        topgene = BedTool.from_dataframe(df.query(top))
        feature = BedTool(input.feature)

        result = topgene.fisher(feature, g=input.genome)

        result_pvalues = "{:.4f},{:.4f},{:.4f},{:.4f},{},{},{},{},{},{}".format(
            result.left_tail,
            result.right_tail,
            result.two_tail,
            result.ratio,
            wildcards.top,
            wildcards.feat,
            wildcards.ctx,
            wildcards.etype,
            wildcards.gtype,
            wildcards.src
        )
       
        with open(output.txt, "w") as text:
            print(result.text.rstrip("\n"), file=text)

        with open(output.dat, "w") as data:
            print(result_pvalues, file=data)

        

rule bedtools_fisher_cat:
    """Note: this rule fails with snakemake due to OSError Argument list
    too long but works from the shell by hand.
    """
    input:
        expand(
            "fisher_{top}-metastable-genes_{feat}_{ctx}_{etype}_{gtype}_{src}.txt",
            top=config["params"]["plot_upset"]["top"],
            feat=features,
            ctx=config["params"]["plot_upset"]["context"],
            etype=config["data"]["genome"]["ecotype"],
            gtype=config["data"]["genome"]["genotype"],
            src=config["data"]["source"]
        )
    output:
        "fig/tests-fisher_overlap-metastable-genes.csv"
    shell:
        "echo 'pvalue (left),pvalue (right),pvalue (two-tail), ratio,quantile,feature,context,ecotype,genotype,source'"
        " >> {output};"
        " cat {input} >> {output}"
