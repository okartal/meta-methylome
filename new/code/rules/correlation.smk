rule correlation_jsdmet_all:
    input:
        div = "div_{ctx}_{etype}_{gtype}_{src}.bed.gz"
    output:
        cor = "spearman-all_{ctx}_{etype}_{gtype}_{src}.csv"
    run:
        div = pd.read_csv(
            input.div, sep="\t", header=0,
            usecols=["JSD_bit_", "5mC", "C"])

        div["MET"] = div["5mC"]/(div["5mC"] + div["C"])
        
        rho, pvalue = stats.spearmanr(div["JSD_bit_"], div["MET"])

        outputstr = "{:.3f},{:.3f},{},{},{},{},{}".format(
            rho,
            pvalue,
            "all",
            wildcards.ctx,
            wildcards.etype,
            wildcards.gtype,
            wildcards.src)

        with open(output.cor, "w") as outputfile:
            print(outputstr, file=outputfile)
        
rule correlation_jsdmet_ctype:
    input:
        div = "{ctype}-lb20ub80_{ctx}_{etype}_{gtype}_{src}.bed"
    output:
        cor = "spearman-{ctype}_{ctx}_{etype}_{gtype}_{src}.csv"
    run:
        div = pd.read_csv(
            input.div, sep="\t", usecols=[3, 8], header=None, names=["JSD", "MET"])
        
        rho, pvalue = stats.spearmanr(div["JSD"], div["MET"])

        outputstr = "{:.3f},{:.3f},{},{},{},{},{}".format(
            rho,
            pvalue,
            wildcards.ctype,
            wildcards.ctx,
            wildcards.etype,
            wildcards.gtype,
            wildcards.src)

        with open(output.cor, "w") as outputfile:
            print(outputstr, file=outputfile)

rule cat_correlation:
    input:
        expand(
            "spearman-{ctype}_{ctx}_{etype}_{gtype}_{src}.csv",
            ctype=["all", "LMC"] + config["data"]["cytosine"]["type"],
            ctx=config["data"]["cytosine"]["context"],
            etype=config["data"]["genome"]["ecotype"],
            gtype=config["data"]["genome"]["genotype"],
            src=config["data"]["source"])
    output:
        "plot-spearman_jsdmet.csv"
    shell:
        "echo 'spearmanr,p-value,cytosine-type,context,ecotype,genotype,source'"
        " >> {output};"
        " cat {input} >> {output}"

