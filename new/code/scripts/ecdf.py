#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Compute the empirical cumulative distribution function.

Given a sorted file of numbers and a support in this file, this script
computes the ECDF in a lazy fashion. It is suited for very big files.

TODO: make support an argument to parse.

"""

import fileinput

# the support over the sample space has to be given as input
support = (x * 0.01 for x in range(0, 101))

output_table = list()

# In the loop over the sorted file, add the line index (i.e. the
# previous count which is the number of elements fulfilling the
# condition value <= x_value) to the output table and reiterate.

x_value = next(support)

for i, line in enumerate(fileinput.input()):
    count = i + 1
    value = float(line.strip())
    if value > x_value:
        output_table.append([x_value, i])
        x_value = next(support)

# After the loop is exhausted, count equals the total count. Assign
# this value to the remaining elements of the support to cover the
# whole sample space in the output table.

output_table.append([x_value, count])

for x_remaining in support:
    output_table.append([x_remaining, count])

# output cumulative frequency

for x, y in output_table:
    print(x, y/count, sep=",")
