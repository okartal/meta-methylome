import sys
import numpy as np
import pandas as pd

input_file = sys.argv[1]

output_columns = "chrom start end name score strand".split()

data = pd.read_table(
    input_file,
    header=0,
    dtype={
        "Transposon_Name": str,
        "orientation_is_5prime": bool,
        "Transposon_min_Start": int,
        "Transposon_max_End": int,
        "Transposon_Family": str,
        "Transposon_Super_Family": str,
    },
)

data["chrom"] = data["Transposon_Name"].str.extract("AT(\d)TE")
data["start"] = data["Transposon_min_Start"]
data["end"] = data["Transposon_max_End"]
data["name"] = data["Transposon_Name"].str.strip()
data["score"] = "."
data["strand"] = data["orientation_is_5prime"].replace(
    to_replace={False: "-", True: "+"}
)
data["superfamily"] = (
    data["Transposon_Super_Family"]
    .str.strip()
    .str.strip('?')
    .replace({"Unassigned": np.nan})
    .astype("category")
)

superfamily = data.dropna().sort_values(["chrom", "start", "end"]).groupby("superfamily")

for name, subdata in superfamily:
    subdata[output_columns].to_csv("feature_TE_" + name.replace("/", "_") + ".bed", sep="\t", header=False, index=False)
