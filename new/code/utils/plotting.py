# plotting helper functions
import altair as alt
import pandas as pd

def plot_ctype(url, encoding, colormap, source):
    """Plot stacked, normalized barchart for C-types.
    """
    
    data = pd.read_csv(url, header=0)
    
    data = data[data["source"]==source]
    
    data["key"] = data.apply(lambda x: encoding[x["context"]][x["C-type"]], axis=1)
    data["context"] = data["context"].replace({"CpG": "CG"})
    
    keys = []
    colors = []
    
    for context, v in encoding.items():
        for ctype, key in v.items():
            keys.append(key)
            colors.append(colormap[context][ctype])

    chart = alt.Chart(data).mark_bar().encode(
        x=alt.X(
            "count:Q",
            stack="normalize",
            axis=None
        ),
        y=alt.Y(
            "context:N",
            sort=["CG", "CHG", "CHH"],
            title=source.replace("-", " ")
        ),
        color=alt.Color(
            "key:N",
            legend=None,
            scale=alt.Scale(domain=keys, range=colors)
        ),
        order=alt.Order("C-key:N", sort="descending")
    )
    
    return chart

def get_chromtracks(url, encoding, colormap, source, chrom):
    """Yields chromtrack data frames with associated keys and colors.
    """
    for context, v in encoding.items():
        for ctype, key in v.items():
            if ctype != "LMC":
                file = url.format(cty=ctype, ctx=context, src=source)
                df = pd.read_csv(file, names=['chrom', 'start', 'end', 'density'], sep="\t")
                yield df.query("chrom=='{}'".format(chrom)), key, colormap[context][ctype]

def chromtrack(url, encoding, colormap, source, chrom):
    """Makes a chromosome track.
    """
    
    dfs, keys, colors = zip(*get_chromtracks(
        url, encoding, colormap, source, chrom))

    data = pd.concat(dfs, keys=keys, names=["key"]).reset_index()

    chart = (alt.Chart(data)
             .mark_bar()
             .encode(
                 x=alt.X("start:N", axis=None),
                 y=alt.Y(
                     "density:Q",
                     stack="center",
                     axis=None
                 ),
                 color=alt.Color(
                     "key:N",
                     legend=None,
                     scale=alt.Scale(domain=keys, range=colors)
                 )
             )
            )
    
    return chart
