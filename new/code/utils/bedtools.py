# bedtools helper functions
import itertools as it
import math

from .stats import additive_smoothing

def smooth(feature, dim=None, alpha=1):
    """Additive smoothing of feature counts.
    """
    counts = [int(c) for c in feature[-dim:]]

    theta = additive_smoothing(counts, alpha=alpha)

    for i, t in enumerate(theta, -dim):
        feature[i] = str(t)

    return feature

def add_met(feature):
    """Add methylation level to shannon div feature.
    """
    m = float(feature[-2]) 
    c = float(feature[-1])
    feature.append(str(m/(m + c)))
    return feature

def coverage_filter(feature, dim=None, min_fraction=0.05):
    """Filter feature by coverage
    """
    counts = [int(c) for c in feature[-dim:]]
    obs_fraction = sum(counts)/len(feature)
    return obs_fraction >= min_fraction

def update_scores(feature, score):
    """Update score field of BED6.
    """
    if feature[3] in score.keys():
        feature[4] = str(score[feature[3]])
    return feature

def random_samples(df, size=None, n=None):
    """Get random samples from dataframe.
    """
    n_samples = math.floor(len(df) / size)
    sample_id = list(it.chain.from_iterable(zip(*it.repeat(range(n_samples), size))))
    draws = 0
    while draws < n:
        df_shuffled = (df
                       .sample(frac=1, random_state=100)[:len(sample_id)]
                       .assign(sample=sample_id)
                       .set_index("sample")
                       .groupby(level="sample"))
        for _, random_sample in df_shuffled:
            if draws < n:
                draws += 1
                yield random_sample

def nearby(a, b, genome=None, flank=0):
    """Find and count features in b nearby features in a.
    """
    bed = (a
           .slop(b=flank, g=genome)
           .map(b, c=[4, 1], o=["collapse", "count"])
           .filter(lambda f: int(f[-1]))
    )

    return bed
