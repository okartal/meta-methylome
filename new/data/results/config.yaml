data:
  bigwig:
    log2ratio:
      zhao2019:
        samples:
          - Col-0
          - adcp1
        signal:
          - H3K9me2
        background:
          - H3
    mean:
      - Col-0_50k_seedling_nuclei_FANS-ATAC-seq
      - Col-0_50k_root_nuclei_FANS-ATAC-seq
      - Col-0_H3
      - Col-0_H3K9me2
      - adcp1_H3
      - adcp1_H3K9me2
  te_superfamilies:
    - DNA
    - DNA_En-Spm
    - DNA_Harbinger
    - DNA_HAT
    - DNA_Mariner
    - DNA_MuDR
    - DNA_Pogo
    - DNA_Tc1
    - LINE
    - LINE_L1
    - LTR_Copia
    - LTR_Gypsy
    - RathE1_cons
    - RathE2_cons
    - RathE3_cons
    - RC_Helitron
    - SINE
  gff:
    - promoter_all
    - promoter_genic
    - promoter_orphan
    - transposable_element
  diversity:
    - met
    - jsd
  source:
    - aerial-part
    - embryo
    - endosperm
    - immature-flower-buds
    - inflorescence
    - root
    - rosette
    - shoot
    - sperm-cell
    - vegetative-nucleus
    - whole-organism
  cytosine:
    context:
      [CpG, CHG, CHH]
    type:
      [HMC, MMC, MSC]
  genome:
    ecotype: Col-0
    genotype: wt
    chromosome:
      nuclear: ['1', '2', '3', '4', '5']
      cytoplasmic: [Mt, Pt]
  shannon:
    names:
      ['#chrom', start, end, JSD_bit_, 'sample size', HMIX_bit_, 5mC, C]
    dtype:
      [str, int, int, float, int, float, int, int]
  bed_ctype:
    names:
      ['#chrom', start, end, JSD_bit_, 'sample size', HMIX_bit_, 5mC, C, MET]
    dtype:
      [str, int, int, float, int, float, int, int, float]
  bed6_prob_ctype:
    names:
      [chrom, start, end, name, score, strand, HMC, MMC, MSC, LMC]
    dtype:
      [str, int, int, str, str, str, float, float, float, float]
  top_msc_genes:
    names:
      [chrom,start,end,name,context,source,HMC,MMC,MSC,LMC,top1pct,top5pct,top10pct]
    dtype:
      [str,int,int,str,str,str,float,float,float,float,bool,bool,bool]

params:
  genomebins:
    - 50000
    - 500000
  plot_profile_te_superfamilies:
    --averageType mean
    --plotType heatmap
    --startLabel "5'" --endLabel "3'" 
    --yMax 1
  plot_cytotype_spearman:
    pvalue: 0.05   
  count_cytotype_features:
    min coverage: 0.05
  prob_cytotype_features:
    pseudocount: 1
  feature_gene:
    slop_kb: [0, 2]
  plot_cytotype_te_superfamilies:
    columns: [chrom, start, end, name, score, strand, HMC, MMC, MSC, LMC]
    usecols: [LMC, MMC, HMC, MSC]
    minimum group size: 5
  plot_chromtrack:
    source:
      - embryo
      - endosperm
      - vegetative-nucleus
      - sperm-cell
      - immature-flower-buds
      - rosette
      - root
    encoding:
      CpG:
        LMC: 1
        MMC: 2
        HMC: 3
        MSC: 4
      CHG:
        LMC: 5
        MMC: 6
        HMC: 7
        MSC: 8
      CHH:
        LMC: 9
        MMC: 10
        HMC: 11
        MSC: 12
  plot_profile_chromstate:
    --averageType mean  --plotType heatmap --startLabel "5'" --endLabel "3'" --yMax 1 --regionsLabel 
    'active, promoter/TSS (1)'
    'inactive, intergenic (2)'
    'active, intragenic (3)'
    'inactive, intergenic (4)'
    'inactive, Polycomb-regulated (5)'
    'inactive, intragenic (6)'
    'active, intragenic (7)'
    'inactive, AT-rich heterochromatin (8)'
    'inactive, GC-rich heterochromatin (9)'
  plot_gviz:
    genes:
      SKP1: AT1G75950
      UBQ13: AT1G65350 # stable insertion of mitochondrial DNA, overlaps CMT2-targeted TE and is close to RdDM-targeted TE that is close to AGL23, top MSG in vegetative sources
      AGL23: AT1G65360
      PHE1: AT1G65330
      AMPSL: AT1G75960
      DUF827: AT5G55860
      MRD1: AT1G53480
      PI4Kg3: AT5G24240 # imprinted gene with high JSD adjacent, exclusively expressed in pollen, adjacent to gene AT5G24260 which behaves like a bottom50 gene according to Araport/jbrowse
      ### genes that have methylation in gene body (chromstate 7) and/or with 2kb (chromstate 8)
      IQD18: AT1G01110
      OEP7: AT3G52420
      DIN10: AT5G20250 # shows DMRs between generations in my pop-epi experiment
      RSM1: AT2G21650 # regulatory element affected by DNAm in Wibowo et al. 2018 PNAS
      # bottom 50 genes in mature pollen, increased intragenic JSD in other tissues
      FKBP12: AT5G64350 # constitutively expressed except in pollen
      F7G19: AT1G09010 # ditto, see NCBI Gene ID: 837422
      TH2: AT5G32470 # ditto
      GroES: AT3G56460 # ditto
      IAR1: AT1G68100
      # ... close to CMT2-targeted TEs
      ADF3: AT5G59880 #
      RPAP2-like: AT5G26760 # RNA polymerase II subunit B1 CTD phosphatase RPAP2-like protein, unknown function
      GSTU19: AT1G78380 # glutathione S-transferase TAU 19
      # ... close to RdDM--targeted TEs
      PBG1: AT1G56450 # 20S proteasome beta subunit G1
      RLP4: AT1G28340 # receptor like protein 4
      DYL1: AT1G28330 # dormancy-associated protein-like 1, alias: DRM1; close to RLP4 with strong expression in carpel, leaf, root but silenced in pollen
      mybHTH: AT2G01060 # myb-like HTH transcriptional regulator family protein, homeodomain-like (also close to CMT2-targeted TE)
      # top 50 close to CMT2
      LRR: AT4G33970
      # MSGs close to RdDM targets
      SIS1: AT1G09245 # Plant self-incompatibility protein S1 family
    color:
      CG: '#74c476'
      CHG: '#fd8d3c'
      CHH: '#9e9ac8'
  top_msc_genes:
    quantiles: [0.90, 0.95, 0.99]
    output columns:
      - chrom
      - start
      - end
      - name
      - context
      - source
      - HMC
      - MMC
      - MSC
      - LMC
      - top1pct
      - top5pct
      - top10pct
  plot_upset:
    context: [CG, CHG, CHH]
    top: [top1pct, top5pct, top10pct]
    sets:
      - "inflorescence"
      - "sperm cell"
      - "vegetative nucleus"
      - "root"
      - "rosette"
    colormap:
      CG: '#74c476'
      CHG: '#fd8d3c'
      CHH: '#9e9ac8'
  feature_gene_expression:
    expression: ["top50", "bottom50"]
    tissue:
      - carpel
      - mature-pollen
      - root
      - vegetative-rosette
  plot_profile_expression:
    --colorList 'white,#74c476' 'white,#fd8d3c' 'white,#9e9ac8'
    --missingDataColor white
    --averageTypeSummaryPlot mean
    --startLabel "5'" --endLabel "3'"
    --regionsLabel 'top 50' 'bottom 50'
    --samplesLabel 'CG' 'CHG' 'CHH'
    --heatmapHeight 10
    --sortUsing mean
  genes_nearby_chromstate:
    flanksize: 2000
    minlength: 100
  hicdata2bedg:
    - Chr1_1_10000000
    - Chr1_18000000_30427671
    - Chr2_1_1000000
    - Chr2_8000000_19698289
    - Chr3_1_10000000
    - Chr3_17000000_23459830
    - Chr4_1_1500000
    - Chr4_6500000_18585056
    - Chr5_16000000_26975502
    - Chr5_1_9000000
  bedtools_map_jsd_genomebin:
    -c 4 -o mean
  bedtools_intersect_hic_sig:
    -filenames -sorted -wa -wb
  plot_circos_top:
    chromosome.index: [1, 2, 3, 4, 5]
    tracks:
      CG:
        - rosetteCG
        - rootCG
        - vegnucleusCG
        - spermCG
        - inflorescenceCG
      CHG:
        - rosetteCHG
        - rootCHG
        - vegnucleusCHG
        - spermCHG
        - inflorescenceCHG
      CHH:
        - rosetteCHH
        - rootCHH
        - vegnucleusCHH
        - spermCHH
        - inflorescenceCHH
    color:
      inflorescenceCG:  "#74c476"
      inflorescenceCHG: "#fd8d3c"
      inflorescenceCHH: "#9e9ac8"
      spermCG:  "#74c476"
      spermCHG: "#fd8d3c"
      spermCHH: "#9e9ac8"
      vegnucleusCG:  "#74c476"
      vegnucleusCHG: "#fd8d3c"
      vegnucleusCHH: "#9e9ac8"
      rootCG:  "#74c476"
      rootCHG: "#fd8d3c"
      rootCHH: "#9e9ac8"
      rosetteCG:  "#74c476"
      rosetteCHG: "#fd8d3c"
      rosetteCHH: "#9e9ac8"
    columns:
      cytoband:
        ["character", "numeric", "numeric", "character", "character"]
      inflorescenceCG:
        ["character", "numeric", "numeric", "numeric"]
      inflorescenceCHG:
        ["character", "numeric", "numeric", "numeric"]
      inflorescenceCHH:
        ["character", "numeric", "numeric", "numeric"]
      spermCG:
        ["character", "numeric", "numeric", "numeric"]
      spermCHG:
        ["character", "numeric", "numeric", "numeric"]
      spermCHH:
        ["character", "numeric", "numeric", "numeric"]
      vegnucleusCG:
        ["character", "numeric", "numeric", "numeric"]
      vegnucleusCHG:
        ["character", "numeric", "numeric", "numeric"]
      vegnucleusCHH:
        ["character", "numeric", "numeric", "numeric"]
      rootCG:
        ["character", "numeric", "numeric", "numeric"]        
      rootCHG:
        ["character", "numeric", "numeric", "numeric"]
      rootCHH:
        ["character", "numeric", "numeric", "numeric"]
      rosetteCG:
        ["character", "numeric", "numeric", "numeric"]        
      rosetteCHG:
        ["character", "numeric", "numeric", "numeric"]
      rosetteCHH:
        ["character", "numeric", "numeric", "numeric"]
  plot_profile_genes:
    --perGroup
    --averageType mean
    --plotType heatmap
    --startLabel "5'" --endLabel "3'" 
    --yMax 1
    --regionsLabel 'genes (no TEG overlap)' 'genes (only TEG overlap)'  'TEG'
  plot_profile_te_targets:
    --perGroup
    --averageType mean
    --startLabel "5'" --endLabel "3'"
    --regionsLabel 'CMT2-targeted TEs' 'RdDM-targeted TEs'
    --samplesLabel 'CG' 'CHG' 'CHH'
    --colors '#74c476' '#fd8d3c' '#9e9ac8' 
  feature_promoters:
    slop: 50
  colormap:
    context:
      CG: '#74c476'
      CHG: '#fd8d3c'
      CHH: '#9e9ac8'      
    cytotype:
      CpG:
        LMC: '#edf8e9'
        MMC: '#bae4b3'
        HMC: '#74c476'
        MSC: '#238b45'
      CHG:
        LMC: '#feedde'
        MMC: '#fdbe85'
        HMC: '#fd8d3c'
        MSC: '#d94701'
      CHH:
        LMC: '#f2f0f7'
        MMC: '#cbc9e2'
        HMC: '#9e9ac8'
        MSC: '#6a51a3'
  deeptools_matrix_te_superfamilies:
    --beforeRegionStartLength 2000 --regionBodyLength 4000 --afterRegionStartLength 2000
    --binSize 50
  deeptools_matrix_genes:
    --beforeRegionStartLength 2000 --regionBodyLength 4000 --afterRegionStartLength 2000
    --binSize 50
  deeptools_matrix_te_targets:
    --beforeRegionStartLength 2000 --regionBodyLength 4000 --afterRegionStartLength 2000
    --binSize 50
  deeptools_matrix_expression:
    --beforeRegionStartLength 2000 --regionBodyLength 4000 --afterRegionStartLength 2000
    --binSize 200
  genes_nearby_targetedTEs:
    flank: 2000
    targets:
      - CMT2
      - RdDM
    candidates:
      - imprinted
      - expression-mature-pollen-bottom50
      - protein-coding-0kb-flank_top5pct-msc_CG_rosette_Col-0_wt
      - protein-coding-0kb-flank_top5pct-msc_CHG_rosette_Col-0_wt
      - protein-coding-0kb-flank_top5pct-msc_CHH_rosette_Col-0_wt
      - protein-coding-0kb-flank_top5pct-msc_CG_root_Col-0_wt
      - protein-coding-0kb-flank_top5pct-msc_CHG_root_Col-0_wt
      - protein-coding-0kb-flank_top5pct-msc_CHH_root_Col-0_wt
      - protein-coding-0kb-flank_non-transposable-element
      - protein-coding-0kb-flank_top5pct-msc_CG_inflorescence_Col-0_wt
      - protein-coding-0kb-flank_top5pct-msc_CHG_inflorescence_Col-0_wt
      - protein-coding-0kb-flank_top5pct-msc_CHH_inflorescence_Col-0_wt
    randomized:
      size:
        imprinted: 236
        expression-mature-pollen-bottom50: 50
        protein-coding-0kb-flank_top5pct-msc_CG_rosette_Col-0_wt: 683
        protein-coding-0kb-flank_top5pct-msc_CHG_rosette_Col-0_wt: 1027
        protein-coding-0kb-flank_top5pct-msc_CHH_rosette_Col-0_wt: 1364
        protein-coding-0kb-flank_top5pct-msc_CG_root_Col-0_wt: 646
        protein-coding-0kb-flank_top5pct-msc_CHG_root_Col-0_wt: 994
        protein-coding-0kb-flank_top5pct-msc_CHH_root_Col-0_wt: 1345
        protein-coding-0kb-flank_top5pct-msc_CG_inflorescence_Col-0_wt: 670
        protein-coding-0kb-flank_top5pct-msc_CHG_inflorescence_Col-0_wt: 1012
        protein-coding-0kb-flank_top5pct-msc_CHH_inflorescence_Col-0_wt: 1352
      draws:
        - 1e4
  reldist_genes_targetedTEs:
    protein-coding-0kb-flank_non-transposable-element:
      color: "#34495e"
      alias: all
      set: ['1', '2', '3', '4', '5', '6']
    imprinted:
      color: "#95a5a6"
      alias: imprinted
      set: ['1']
    expression-mature-pollen-bottom50:
      color: "#3498db"
      alias: bottom 50 mature pollen
      set: ['2']
    expression-mature-pollen-top50:
      color: "#e74c3c"
      alias: top 50 mature pollen
      set: ['2']
    expression-vegetative-rosette-bottom50:
      color: "#3498db"
      alias: bottom 50 rosette
      set: ['6']
    expression-vegetative-rosette-top50:
      color: "#e74c3c"
      alias: top 50 rosette
      set: ['6']
    protein-coding-0kb-flank_top5pct-msc_CG_rosette_Col-0_wt:
      color: '#74c476'
      alias: MSG CG rosette
      set: ['3']
    protein-coding-0kb-flank_top5pct-msc_CHG_rosette_Col-0_wt:
      color: '#fd8d3c'
      alias: MSG CHG rosette
      set: ['3']
    protein-coding-0kb-flank_top5pct-msc_CHH_rosette_Col-0_wt:
      color: '#9e9ac8'
      alias: MSG CHH rosette
      set: ['3']
    nearby-chromstate1:
      color: "#95a5a6"
      alias: nearby chromstate 1
      set: ['4']
    nearby-chromstate2:
      color: "#95a5a6"
      alias: nearby chromstate 2
      set: ['4']
    nearby-chromstate3:
      color: "#95a5a6"
      alias: nearby chromstate 3
      set: ['4']
    nearby-chromstate4:
      color: "#95a5a6"
      alias: nearby chromstate 4
      set: ['4']
    nearby-chromstate5:
      color: "#95a5a6"
      alias: nearby chromstate 5
      set: ['4']
    nearby-chromstate6:
      color: "#95a5a6"
      alias: nearby chromstate 6
      set: ['4']
    nearby-chromstate7:
      color: "#95a5a6"
      alias: nearby chromstate 7
      set: ['4']
    nearby-chromstate8:
      color: "#9b59b6"
      alias: nearby chromstate 8
      set: ['4']
    nearby-chromstate9:
      color: "#3498db"
      alias: nearby chromstate 9
      set: ['4']
    protein-coding-0kb-flank_top5pct-msc_CG_root_Col-0_wt:
      color: '#74c476'
      alias: MSG CG root
      set: ['5']
    protein-coding-0kb-flank_top5pct-msc_CHG_root_Col-0_wt:
      color: '#fd8d3c'
      alias: MSG CHG root
      set: ['5']
    protein-coding-0kb-flank_top5pct-msc_CHH_root_Col-0_wt:
      color: '#9e9ac8'
      alias: MSG CHH root
      set: ['5']
  hic_mean_domains:
    CSD:
      sign: "< 0"
    LSD:
      sign: "> 0"

threads:
  deeptools_matrix_expression: 16
  deeptools_matrix_te_superfamilies: 16
  deeptools_matrix_genes: 16
  deeptools_matrix_te_targets: 16


